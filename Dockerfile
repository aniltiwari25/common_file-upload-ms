FROM php:8.1-fpm
ENV LIBRDKAFKA_VERSION v1.7.0
RUN apt-get update -y \
    && apt-get install -y nginx git nano zip wget dpkg cron supervisor libpng-dev  \
    zip git nano nginx htop supervisor librdkafka-dev wget cron vim iputils-ping

RUN docker-php-ext-install pdo_mysql \
    && docker-php-ext-install opcache \
    && apt-get install libicu-dev -y \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && apt-get remove libicu-dev icu-devtools -y \
    && docker-php-ext-install pcntl


# Fix Permission
RUN usermod -u 1000 www-data

RUN apt-get update -y \
    && apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libgd-dev libmagickwand-dev --no-install-recommends \
    && cd /tmp \
        && git clone \
            --branch ${LIBRDKAFKA_VERSION} \
            --depth 1 \
            https://github.com/edenhill/librdkafka.git \
        && cd librdkafka \
        && ./configure \
        && make \
        && make install \
        && pecl install rdkafka \
        && docker-php-ext-enable rdkafka \
        && rm -rf /tmp/librdkafka

RUN docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN printf "\n" | pecl install imagick

RUN docker-php-ext-enable imagick


COPY . /var/www/html
COPY /docker/etc/upload.ini /usr/local/etc/php/conf.d/upload.ini
WORKDIR /var/www/html

RUN rm /etc/nginx/sites-enabled/default

##RUN echo 10.0.0.245 qa-inward-files-upload.hellogroup.co.za > /etc/hosts

COPY /docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin/ --filename=composer
ENV COMPOSER_ALLOW_SUPERUSER 1

RUN composer install --prefer-dist

COPY .env.example .env
RUN php artisan key:generate
RUN mkdir -p ./storage/framework/sessions
RUN mkdir -p ./storage/logs
RUN chmod +766 -R ./storage/
RUN chmod +x ./entrypoint

ENTRYPOINT ["./entrypoint"]

EXPOSE 443
