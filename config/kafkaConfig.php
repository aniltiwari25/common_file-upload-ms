<?php

return [
    'kafka' => [
                'BROKER_URL' => env('KAFKA_BROKERS', 'pkc-l6wr6.europe-west2.gcp.confluent.cloud:9092'),
                'TOPIC_NAME' => env('KAFKA_TOPIC_NAME', 'upload-doc'),
                'PROTOCOL' => env('KAFKA_SECURITY_PROTOCOL','SASL_SSL'),
                'MECHANISMS' => env('KAFKA_SASL_MECHANISMS','PLAIN'),
                'USERNAME' => env('KAFKA_USERNAME'),
                'KAFKA_PASSWORD' => env('KAFKA_PASSWORD'),
                'KAFKA_DEBUG_LOGS' => env('KAFKA_DEBUG_LOGS',false),
                'KAFKA_REFRESH_RATE' => env('KAFKA_REFRESH_RATE',1)
            ]
];
