<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientDetails extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id_client';
    protected $dates = [
        'added_on'
    ];

    protected $table = 'client_details';

    protected $fillable = [
        'id_client', 'client_name', 'added_on','s3_upload_folder_name','callback_url','callback_url_jwt_token', 'send_callback_on_kafka' , 'kafka_topic_name'
    ];


}
