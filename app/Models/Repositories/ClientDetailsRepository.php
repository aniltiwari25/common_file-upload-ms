<?php

namespace App\Models\Repositories;

use App\Models\ClientDetails;

class ClientDetailsRepository
{
    public function __construct()
    {

    }

    public function getClientDetailsById($clientId){
        return ClientDetails::find($clientId);
    }
}
