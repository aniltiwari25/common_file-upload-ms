<?php

namespace App\Models\Repositories;

use App\Models\HelloDocSubTypes;

class HelloDocSubTypesRepository
{
    public function getHelloSubDocTypeById($fileDocTypeId){
        return HelloDocSubTypes::find($fileDocTypeId);
    }
}
