<?php

namespace App\Models\Repositories;

use App\Models\FileUploadDetails;

class FileUploadDetailsRepository
{
    public function saveFileUploadRequest($fileUploadRequest)
    {
        return FileUploadDetails::create($fileUploadRequest) ;

    }

    public function getFileUploadDetailsByIdAndCustomerId($id , $customerId)
    {
        return FileUploadDetails::where(['id' => $id , 'customer_id' => $customerId])->first();
    }
}
