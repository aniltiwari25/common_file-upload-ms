<?php

namespace App\Models\Repositories;

use App\Models\HelloDocTypes;

class HelloDocTypesRepository
{

    public function getHelloDocTypeById($fileDocTypeId){
        return HelloDocTypes::find($fileDocTypeId);
    }
}
