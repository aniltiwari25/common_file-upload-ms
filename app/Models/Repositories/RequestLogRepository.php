<?php

namespace App\Models\Repositories;

use App\Models\RequestLog;

class RequestLogRepository
{

    public function createRequest($requestLogModelArr): RequestLog
    {
        $requestLogModel  =  new RequestLog();

        $requestLogModel->request_url = $requestLogModelArr['request_url'];
        $requestLogModel->request_body = $requestLogModelArr['request_body'];
        $requestLogModel->id_client = $requestLogModelArr['id_client'];

        $requestLogModel->save();
        return $requestLogModel;
    }

}
