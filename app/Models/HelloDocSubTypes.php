<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HelloDocSubTypes extends Model
{

    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'HelloDocSubTypes';

    public function helloDocTypes()
    {
        return $this->hasOne('App\Models\HelloDocTypes','fkHelloDocTypeID');
    }


}
