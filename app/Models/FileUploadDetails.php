<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileUploadDetails extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id';
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $table = 'file_upload_details';

    protected $fillable = [
        'id', 'request_id', 'client_id', 'customer_id', 'client_unique_id', 'upload_placeholder_details', 'file_format',
        'upload_s3_path', 's3_folder_name', 'created_at', 'updated_at','file_doc_type_id','file_sub_doc_type_id'
    ];
}
