<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    public $timestamps = true;
    protected $primaryKey = 'id_request';

    /**
     * @var mixed|string
     */
    private $request_url;
    /**
     * @var false|mixed|string
     */
    private $request_body;
    /**
     * @var mixed
     */
    private $id_client;

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $table = 'request_log';

    protected $fillable = [
        'id_request', 'request_url', 'request_body', 'id_client', 'response_sent', 'created_at', 'updated_at'
    ];

}
