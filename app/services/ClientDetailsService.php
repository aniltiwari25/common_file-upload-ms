<?php

namespace App\services;

use App\Constants\ErrorResponse;
use App\Exceptions\CustomException;
use App\Models\Repositories\ClientDetailsRepository;
use Symfony\Component\HttpFoundation\Response;

class ClientDetailsService
{

    protected $repository;

    public function __construct()
    {
        $this->repository = new ClientDetailsRepository();
    }

    public function getClientDetailsById($clientId){
        $client =  $this->repository->getClientDetailsById($clientId);
        if (empty($client)) {
            // Throw exception
            throw new CustomException(ErrorResponse::INVALID_CLIENT_ID,
                Response::HTTP_BAD_REQUEST,Response::HTTP_BAD_REQUEST);
        }
        return $client;
    }
}
