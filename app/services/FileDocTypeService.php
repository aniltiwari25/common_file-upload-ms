<?php

namespace App\services;

use App\Constants\ErrorResponse;
use App\Exceptions\CustomException;
use App\Models\Repositories\HelloDocTypesRepository;
use Symfony\Component\HttpFoundation\Response;


class FileDocTypeService
{

    protected $repository;

    public function __construct()
    {
        $this->repository = new HelloDocTypesRepository();
    }

    public function validateFileDocType($fileDocTypeId){
        $docTypeDetails =  $this->repository->getHelloDocTypeById($fileDocTypeId);
        if (empty($docTypeDetails)) {
            // Throw exception
            throw new CustomException(ErrorResponse::INVALID_DOCTYPE_ID,
                Response::HTTP_BAD_REQUEST,Response::HTTP_BAD_REQUEST);
        }
        return $docTypeDetails;
    }
}
