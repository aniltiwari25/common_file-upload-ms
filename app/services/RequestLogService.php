<?php

namespace App\services;

use App\Models\Repositories\RequestLogRepository;
use App\Models\RequestLog;

class RequestLogService
{
    protected $repository;

    public function __construct()
    {
        $this->repository = new RequestLogRepository();
    }

    public function saveRequestLog($requestLog): RequestLog
    {
        return $this->repository->createRequest($requestLog);
    }
}
