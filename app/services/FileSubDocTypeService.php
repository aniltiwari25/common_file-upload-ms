<?php

namespace App\services;

use App\Constants\ErrorResponse;
use App\Exceptions\CustomException;
use App\Models\Repositories\HelloDocSubTypesRepository;
use Symfony\Component\HttpFoundation\Response;


class FileSubDocTypeService
{

    protected $repository;

    public function __construct()
    {
        $this->repository = new HelloDocSubTypesRepository();
    }

    public function validateFileSubDocType($fileDocTypeId){
        $docSubTypeDetails =  $this->repository->getHelloSubDocTypeById($fileDocTypeId);
        if (empty($docSubTypeDetails)) {
            // Throw exception
            throw new CustomException(ErrorResponse::INVALID_SUB_DOCTYPE_ID,
                Response::HTTP_BAD_REQUEST,Response::HTTP_BAD_REQUEST);
        }
        return $docSubTypeDetails;
    }
}
