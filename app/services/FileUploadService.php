<?php

namespace App\services;

use App\Constants\ErrorResponse;
use App\Constants\HttpStatusCodesConsts;
use App\Exceptions\CustomException;
use App\Models\Repositories\FileUploadDetailsRepository;
use Validator;

class FileUploadService
{

    public function __construct()
    {
        $this->clientDeatilService = new ClientDetailsService();
        $this->docTypeService = new FileDocTypeService();
        $this->docSubTypeService = new FileSubDocTypeService();
        $this->fileUploadDeatilsRepo = new FileUploadDetailsRepository();
    }

    /**
     * @throws CustomException
     */
    public function validateRequestAndGetClientDetails($inputParams)
    {
        $rules       = [  'fkHelloDocTypeID'    => 'required',
                            'clientId' => 'required' ,
                            'customerId' => 'required' ,
                            'clientUniqueId' => 'required'
                       ];

        $customMessages = [
            'required' => 'The :attribute field is required.'
        ];

        $validator   = Validator::make( $inputParams, $rules , $customMessages);

        if ($validator->fails())
        {
            $errorMessages = current( $validator->messages() );
            foreach ($errorMessages as $key => $value)
            {
                throw new CustomException(current($value)  , HttpStatusCodesConsts::HTTP_BAD_REQUEST, HttpStatusCodesConsts::HTTP_BAD_REQUEST);
            }
        }

        $client = $this->clientDeatilService->getClientDetailsById($inputParams['clientId']);

        /*
         * VALIDATE DOCTYPE & SUB DOCTYPE
         */
        $isValidDocTypeId = $this->docTypeService->validateFileDocType($inputParams['fkHelloDocTypeID']);

        if( isset($inputParams['fkHelloDocSubTypeID']) && !empty( $inputParams['fkHelloDocSubTypeID'] ) )
        {
            $isValidSubDocTypeId = $this->docSubTypeService->validateFileSubDocType($inputParams['fkHelloDocSubTypeID']);
        }
        return $client;
    }


    /**
     * @throws CustomException
     */
    public function validateFileUploadRequest($request)
    {
        $maxSize        = env('MAX_FILE_SIZE', 8) *1024;

        if(!$request->file('upload_file'))
        {
            throw new CustomException(ErrorResponse::FILE_NOT_UPLOADED_MSG,
                HttpStatusCodesConsts::HTTP_BAD_REQUEST,
                HttpStatusCodesConsts::HTTP_BAD_REQUEST);
        }
        $fileObject    = $request->file('upload_file');

        if($fileObject != null)
        {
            $rules          = [  'upload_file'  => 'required|mimes:xlsx,jpeg,jpg,png,pjpeg,pdf,xls,xlsx,doc,docx|max:'.$maxSize ];
            $validator      = Validator::make( $request->all(), $rules );

            if ($validator->fails())
            {
                $errorMessages = current( $validator->messages() );
                foreach ($errorMessages as $key => $value)
                {
                    throw new CustomException(current($value)  , HttpStatusCodesConsts::HTTP_BAD_REQUEST, HttpStatusCodesConsts::HTTP_BAD_REQUEST);
                }
            }
        }
        else
        {
            throw new CustomException(ErrorResponse::FILE_NOT_UPLOADED_MSG,
                HttpStatusCodesConsts::HTTP_BAD_REQUEST,
                HttpStatusCodesConsts::HTTP_BAD_REQUEST);
        }

    }


    public function saveFileUploadRequest($fileUploadRequest)
    {
        return $this->fileUploadDeatilsRepo->saveFileUploadRequest($fileUploadRequest);
    }

}
