<?php

namespace App\services;

use App\Constants\ErrorResponse;
use App\Exceptions\CustomException;
use App\Models\Repositories\FileUploadDetailsRepository;
use Symfony\Component\HttpFoundation\Response;

class FileUploadDetailsService
{

    protected $repository;

    public function __construct()
    {
        $this->repository = new FileUploadDetailsRepository();
    }

    public function getFileUploadDetailsByIdAndCustomerId($id , $customerId){
        $uploadDetails =  $this->repository->getFileUploadDetailsByIdAndCustomerId($id , $customerId);
        if (empty($uploadDetails)) {
            // Throw exception
            throw new CustomException(ErrorResponse::INVALID_DETAILS,
                Response::HTTP_BAD_REQUEST,Response::HTTP_BAD_REQUEST);
        }
        return $uploadDetails;
    }
}
