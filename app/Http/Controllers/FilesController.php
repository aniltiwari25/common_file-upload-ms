<?php

namespace App\Http\Controllers;


use App\Constants\HttpStatusCodesConsts;
use App\Exceptions\CustomException;

use App\Http\Helpers\HtmlToPdfHelper;
use App\Http\Helpers\ImageHelper;
use App\Http\Helpers\SThreeHelper;
use App\Models\ClientDetails;
use App\Models\FileUploadDetails;
use App\Models\RequestLog;
use App\services\ClientDetailsService;
use App\services\FileUploadDetailsService;
use App\services\FileUploadService;
use App\services\RequestLogService;
use Illuminate\Http\Request;
use Junges\Kafka\Facades\Kafka;
use Junges\Kafka\Message\Message;
use Validator;

class FilesController extends Controller
{

    protected $clientDeatilService;
    private RequestLogService $requestLogService;
    private FileUploadService $fileUploadService;
    private FileUploadDetailsService $fileUploadDeatilsService;

    public function __construct()
    {
        $this->fileUploadService = new FileUploadService();
        $this->requestLogService = new RequestLogService();
        $this->clientDeatilService = new ClientDetailsService();
        $this->fileUploadDeatilsService = new FileUploadDetailsService();
    }

    /**
     * @throws \App\Exceptions\CustomException
     */
    public function uploadFile(Request $request)
    {
        $inputParams = $request->all();

        /*
         * VALIDATE  REQUEST & CLIENT ID -
         */
        $clientDetails = $this->fileUploadService->validateRequestAndGetClientDetails($inputParams);

        $financePartnerId        = env('FINANCE_PARTNER_ID', 2);
        if($financePartnerId != $inputParams['clientId']){
            $this->fileUploadService->validateFileUploadRequest($request);
        }

        /*
         * CREATE REQUEST -
         */
        $requestLogArr = ['request_url' => $request->fullUrl(),
            'request_body' => json_encode($inputParams, JSON_UNESCAPED_SLASHES),
            'id_client' => $inputParams['clientId']
        ];
        $requestLog = $this->requestLogService->saveRequestLog($requestLogArr);

        /*
         * PREPARE S3 UPLAOD REQ -
         */

        $xJwt = $request->header('X-JWT-Assertion');
        $fkCustomerID = $inputParams['customerId'];
        $fkHelloDocTypeID = isset($inputParams['fkHelloDocTypeID']) && !empty($inputParams['fkHelloDocTypeID']) ? $inputParams['fkHelloDocTypeID'] : NULL;
        $fkHelloDocSubTypeID = isset($inputParams['fkHelloDocSubTypeID']) && !empty($inputParams['fkHelloDocSubTypeID']) ? $inputParams['fkHelloDocSubTypeID'] : NULL;

        $fileObject = $request->file('upload_file');
        $fileExtension = $fileObject->extension();
        $imgData = file_get_contents($fileObject->getRealPath());
        $fileName = (string)$clientDetails->id_client . "_" . (string)$requestLog->id_request . "_" . (string)$inputParams['clientUniqueId'] . "_" . $fileObject->getClientOriginalName();
        $fileDesc = $fileName;
        $folderName = $clientDetails->s3_upload_folder_name;
        $bucketName = $clientDetails->s3_bucket_name;
        $mimeType = $fileObject->getClientMimeType();

        try {

            $s3bucketDetails = [
                'key' => $clientDetails->s3_key,
                'secret' => $clientDetails->s3_secret,
                'region' => $clientDetails->s3_region,
                'bucket' => $bucketName,
                's3_path' => $folderName,
            ];

            $res = SThreeHelper::uploadToS3($xJwt, $fkCustomerID, $fkHelloDocTypeID, $fkHelloDocSubTypeID, $fileExtension, $imgData, $fileName, $fileDesc, $folderName, $s3bucketDetails, false);

            /*
             * INSERT DETAILS IN FILE REQUEST TABLE & SEND CALLBACK -
             */
            \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Successful Upload S3 Folder Path - " . json_encode($res, JSON_UNESCAPED_SLASHES));

            $fileUploadDetails = array();

            $fileUploadDetails['request_id'] = $requestLog->id_request;
            $fileUploadDetails['client_id'] = $inputParams['clientId'];
            $fileUploadDetails['customer_id'] = $fkCustomerID;
            $fileUploadDetails['client_unique_id'] = $inputParams['clientUniqueId'];
            $fileUploadDetails['file_format'] = $mimeType;
            $fileUploadDetails['s3_folder_name'] = $res['s3Url'];
            $fileUploadDetails['file_doc_type_id'] = $fkHelloDocTypeID;
            $fileUploadDetails['file_sub_doc_type_id'] = $fkHelloDocSubTypeID;

            $fileUploadObj = $this->fileUploadService->saveFileUploadRequest($fileUploadDetails);

            // Add logic to call the call back Url -

            $inputParams['s3Path'] = $res['s3Url'];
            $inputParams['fileUploadId'] = $fileUploadObj->id;
            $inputParams['fileFormat'] = $mimeType;

            unset($inputParams['upload_file']);
            $requestLog->response_sent = json_encode($inputParams, JSON_UNESCAPED_SLASHES);
            $requestLog->save();

            if ($clientDetails->send_callback_on_kafka) {
                // CODE TO CALL KAFKA QUEUE

                $message = new Message(
                    headers: ['__TypeId__' => 'uploadDoc'],
                    body: $inputParams,
                    key: $inputParams['clientUniqueId']
                );

                $kafkaConfig = [
                    'security.protocol' => config("kafkaConfig.kafka.PROTOCOL"),
                    'sasl.mechanisms' => config("kafkaConfig.kafka.MECHANISMS"),
                    'sasl.username' => $clientDetails->kafka_username,
                    'sasl.password' => $clientDetails->kafka_password,
                    'topic.metadata.refresh.interval.ms' => config("kafkaConfig.kafka.KAFKA_REFRESH_RATE"),
                    'compression.codec' => 'none',
                    'compression.type' => 'none'
                ];

                $producer = Kafka::publishOn($clientDetails->kafka_topic_name, config("kafkaConfig.kafka.BROKER_URL"))
                    ->withConfigOptions($kafkaConfig)
                    ->withMessage($message)
                    ->withDebugEnabled(config("kafkaConfig.kafka.KAFKA_DEBUG_LOGS"));

                $producer->send();

            } else {
                // CODE TO CALL CALLBACK API

                $headers = ['Content-Type' => 'application/json', 'X-JWT-Assertion' => $clientDetails['callback_url_jwt_token']];

                \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Callback Request Headers " . json_encode($headers, JSON_UNESCAPED_SLASHES));
                \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Callback Request Url " . $clientDetails['callback_url']);
                \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Callback Request Body " . json_encode($inputParams, JSON_UNESCAPED_SLASHES));

                //$callBackApiResponse  = NetworkRequests::postRequest($clientDetails['callback_url'] , $inputParams , $headers);

                // \Log::info(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Callback API RESPONSE ". json_encode($callBackApiResponse,JSON_UNESCAPED_SLASHES));
            };

            return response($inputParams, HttpStatusCodesConsts::HTTP_OK);

        } catch (\Exception $e) {
            if ($e instanceof HttpException) {
                $statusCode = $e->getStatusCode();

                $errorMessage = $e->getMessage();
            } else {
                $statusCode = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR;

                $errorMessage = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR_STRING;
            }

            \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " File Upload Exception " . print_r($e->getMessage(), true));

            throw new CustomException($e->getMessage(),
                HttpStatusCodesConsts::HTTP_EXPECTATION_FAILED,
                HttpStatusCodesConsts::HTTP_EXPECTATION_FAILED);
        }
    }


    /**
     * @throws CustomException
     */
    public function fetchFile($fkCustomerID, $fileId, Request $request)
    {
        $inputParams = $request->all();

        $customerFileDetails = $this->fileUploadDeatilsService->getFileUploadDetailsByIdAndCustomerId($fileId,$fkCustomerID);
        //print_r($customerFileDetails);
        try
        {
            $signedUrl      = isset( $inputParams['signedUrl'] ) ? $inputParams['signedUrl'] : false;

            $xJwt           = $request->header('X-JWT-Assertion');

            \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Info - SignedUrl -  $signedUrl  |   xJwt -  $xJwt   | FileNmae -  ".$customerFileDetails->s3_folder_name );

            $clientDetails = $this->clientDeatilService->getClientDetailsById($customerFileDetails->client_id);

            $s3bucketDetails = [
                'key' => $clientDetails->s3_key,
                'secret' => $clientDetails->s3_secret,
                'region' => $clientDetails->s3_region,
                'bucket' => $clientDetails->s3_bucket_name,
                's3_path' => $clientDetails->s3_upload_folder_name
            ];

            $s3Response     = SThreeHelper::fetchS3Files( $customerFileDetails->s3_folder_name,$signedUrl,$s3bucketDetails );

            if($signedUrl)
            {
                return response($s3Response, HttpStatusCodesConsts::HTTP_OK);
            }
            else
            {
                if(str_contains($customerFileDetails->file_format, "image"))
                {
                    $response = response( $s3Response['imageData'] );

                    $response->header('Content-Type', $customerFileDetails->file_format);

                    return $response;
                }
                else
                {
                    $file               = '/tmp/tempFile.pdf';

                    file_put_contents($file, $s3Response['imageData']);

                    if (file_exists( $file ) )
                    {
                        header( "Content-Type: " .  $customerFileDetails->file_format );

                        readfile($file);

                        \File::delete($file);

                        exit;
                    }

                }
                /*                else
                {
                   // $res = ImageHelper::createImage(  $s3Response['imageData']);

                    $response = response( $s3Response['imageData'] );

                    $response->header('Content-Type', $customerFileDetails->file_format);

                    return $response;
                }
                */
            }

        }
        catch (\Exception $e)
        {
            if ($e instanceof HttpException) {
                $statusCode = $e->getStatusCode();

                $errorMessage = $e->getMessage();
            } else {
                $statusCode = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR;

                $errorMessage = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR_STRING;
            }

            \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Exception ".print_r($e->getMessage(),true));

            return response (['errorMessage' => $errorMessage], $statusCode);
        }
    }


    public function generatePdf( $clientId, $customerId, Request $request )
    {
        $inputParams = $request->all();

        $rules       = [  'data'    => 'required', 'templateName' => 'required' ];

        $validator   = Validator::make( $inputParams, $rules );

        if ($validator->fails())
        {
            $errorMessages = current( $validator->messages() );
            foreach ($errorMessages as $key => $value)
            {
                return response( ['errorMessage' => current($value) ], HttpStatusCodesConsts::HTTP_BAD_REQUEST );
            }
        }

        $dataObj            = $inputParams['data'];

        $templateName       = $inputParams['templateName'];

        if( empty( $dataObj) )
        {
            return response( ['errorMessage' => 'data obj cannot be empty' ], HttpStatusCodesConsts::HTTP_BAD_REQUEST );
        }

        // GET CLIENT DETAILS -
        $clientDetails               =   current(
            ClientDetails::where('id_client', $clientId)
                ->get()
                ->toArray()
        );

        if(empty($clientDetails))
        {
            \Log::error(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Error Invalid Client Id ". HttpStatusCodesConsts::HTTP_NOT_FOUND );

            return response( ['errorMessage' => 'Invalid Client Id' ], HttpStatusCodesConsts::HTTP_NOT_FOUND );
        }

        $folderName = $clientDetails['s3_upload_folder_name'];

        $fkCustomerID                = $customerId;

        $fileExtension               = 'pdf';

        $mimeType                    = 'application/pdf';

        $fkHelloDocTypeID            = NULL;

        $fkHelloDocSubTypeID         = NULL;

        // Saving the request -
        $requestLog  =  new RequestLog();

        $requestLog->request_url = $request->fullUrl();
        $requestLog->request_body = json_encode($inputParams , JSON_UNESCAPED_SLASHES);
        $requestLog->id_client = $clientId;

        $requestLog->save();

        /* validate fkHelloDocSubTypeID */
        if( isset($inputParams['fkHelloDocSubTypeID']) && !empty( $inputParams['fkHelloDocSubTypeID'] ) )
        {
            $subDocTypes = $this->validateFkHelloSubDocType($inputParams['fkHelloDocSubTypeID']);

            if( !empty( $fkHelloDocSubTypeID ) )
            {
                if( $subDocTypes['id'] != $fkHelloDocSubTypeID )
                {
                    return response( ['errorMessage' => 'fkHelloDocSubTypeID mismatch ' ], HttpStatusCodesConsts::HTTP_BAD_REQUEST );
                }
            }

            $fkHelloDocTypeID            = $subDocTypes['fkHelloDocTypeID'];

            $fkHelloDocSubTypeID         = $subDocTypes['id'];
        }

        /* validate fkHelloDocTypeID */
        if( isset($inputParams['fkHelloDocTypeID']) && !empty( $inputParams['fkHelloDocTypeID'] ) )
        {

            $HelloDocType = $this->validateFkHelloDocType($inputParams['fkHelloDocTypeID']);

            if( !empty($fkHelloDocTypeID) )
            {
                if( $HelloDocType['id'] != $fkHelloDocTypeID )
                {
                    return response( ['errorMessage' => 'fkHelloDocTypeID mismatch ' ], HttpStatusCodesConsts::HTTP_NOT_FOUND );
                }
            }

            $fkHelloDocTypeID         = $HelloDocType['id'];
        }

        //$fileDesc   = $templateName.'_'.Carbon::now()->timestamp.'.'.$fileExtension;

        if(isset( $inputParams['fileName'] ) && ( $inputParams['fileName'] ) )
        {
            $fileDesc = $inputParams['fileName'];
        }

        $fileName   = $fileDesc;

        /* fetch Customer Signature */

        if(isset( $inputParams['signatureOfCustomer'] ) && $inputParams['signatureOfCustomer'] )
        {

            // may come in future
        }

        /* fetch Customer Signature Ends */


        /* fetch Agent Signature */
        if(isset( $inputParams['signatureOfAgent'] ) && ( $inputParams['signatureOfAgent'] ) )
        {
            // may come in future
        }

        /* fetch Agent Signature Ends */

        /**
         * Upload to S3 Bucket
         */
        try
        {

            $dataObj['SIGNATURE']             =  isset( $customerSignatureHtml ) ? $customerSignatureHtml : '';

            $dataObj['AGENTSIGNATURE']        =  isset( $agentSignatureHtml ) ? $agentSignatureHtml : '';

            if(isset($dataObj['qrCode']))
            {
                $image = \QrCode::format('png')
                    // ->merge('images/favicon.png', 0.4, true)
                    ->color(246,137,30)->size(100)->errorCorrection('H')
                    ->generate($dataObj['qrCode']);

                $dataObj['encodedImage'] = base64_encode($image);
            }

            $pdfData                          = HtmlToPdfHelper::getPdfStream($templateName, $dataObj );

            $xJwt                             = $request->header('X-JWT-Assertion');

            //$folderName    = env('S3_BUCKET_RECEPIENT_FOLDER_NAME','recipients');

            $expireDoc     = false;

            \Log::info(__CLASS__." ".__FUNCTION__." Folder  Name " .$folderName);
            $s3bucketDetails = [
                'key' => $clientDetails['s3_key'],
                'secret' => $clientDetails['s3_secret'],
                'region' => $clientDetails['s3_region'],
                'bucket' => $clientDetails['s3_bucket_name'],
                's3_path' => $clientDetails['s3_upload_folder_name']
            ];

            //$res     = SThreeHelper::uploadToS3( $xJwt,$fkCustomerID, $fkHelloDocTypeID,$fkHelloDocSubTypeID, $fileExtension, $pdfData, $fileName,$fileDesc,$folderName,$expireDoc);
            $res = SThreeHelper::uploadToS3($xJwt, $fkCustomerID, $fkHelloDocTypeID, $fkHelloDocSubTypeID, $fileExtension, $pdfData, $fileName, $fileDesc, $folderName, $s3bucketDetails, false);
            \Log::info(__CLASS__." ".__FUNCTION__.' s3_path => '.$res['s3Url'] );

            //$returnSignedUrl                  = isset($inputParams['signedUrl']) ? true : false;

            $s3Response             = SThreeHelper::fetchS3Files( $res['s3Url'], true, $s3bucketDetails );

            $fileUploadDetails = array();

            $fileUploadDetails['request_id']  = $requestLog->id_request ;
            $fileUploadDetails['client_id']  = $clientId;
            $fileUploadDetails['customer_id']  = $fkCustomerID;
            $fileUploadDetails['client_unique_id']  = $inputParams['clientUniqueId'];
            $fileUploadDetails['file_format']  = $mimeType;
            $fileUploadDetails['s3_folder_name']  = $res['s3Url'];
            $fileUploadDetails['file_doc_type_id']  = $fkHelloDocTypeID;
            $fileUploadDetails['file_sub_doc_type_id']  = $fkHelloDocSubTypeID;
            $fileUploadDetails['upload_placeholder_details']  = json_encode($dataObj,JSON_UNESCAPED_SLASHES);
            $fileUploadDetails['upload_s3_path']  = $s3Response['imageData'];

            $fileUploadObj = FileUploadDetails::create($fileUploadDetails) ;

            // Add logic to call the call back Url -

            $headers = ['Content-Type' => 'application/json', 'X-JWT-Assertion' => $clientDetails['callback_url_jwt_token']];

            \Log::info(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Callback Request Headers ". json_encode($headers,JSON_UNESCAPED_SLASHES));

            $inputParams['s3Path']   =  $res['s3Url'];
            $inputParams['fileUploadId']   =  $fileUploadObj->id;
            $inputParams['fileFormat']   =  $mimeType;
            $inputParams['uplaodedImageUrl'] = $s3Response['imageData'];
            $requestLog->response_sent  = json_encode($inputParams, JSON_UNESCAPED_SLASHES);
            $requestLog->save();

            // \Log::info(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Callback Request Url ".$clientDetails['callback_url'] );
            \Log::info(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Callback Request Body ". json_encode($inputParams,JSON_UNESCAPED_SLASHES));

            //$callBackApiResponse  = NetworkRequests::postRequest($clientDetails['callback_url'] , $inputParams , $headers);

            // \Log::info(__CLASS__." ".__FUNCTION__." " . __LINE__ ." Callback API RESPONSE ". json_encode($callBackApiResponse,JSON_UNESCAPED_SLASHES));

            //if($callBackApiResponse->getStatusCode() != HttpStatusCodesConsts::HTTP_OK)
            //{
            return response( $inputParams, HttpStatusCodesConsts::HTTP_OK );

        }
        catch (\Exception $e)
        {
            if ($e instanceof HttpException) {
                $statusCode = $e->getStatusCode();

                $errorMessage = $e->getMessage();
            } else {
                $statusCode = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR;

                $errorMessage = HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR_STRING;
            }

            \Log::error(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Exception ".print_r($e->getMessage(),true));

            return response (['errorMessage' => $errorMessage], $statusCode);
        }
    }

}
