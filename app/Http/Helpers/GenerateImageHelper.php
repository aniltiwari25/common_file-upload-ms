<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kirit
 * Date: 12/03/2018
 * Time: 9:35 AM
 */

namespace App\Http\Helpers;

use Intervention\Image\Facades\Image;
use App\Constants\HttpStatusCodesConsts;
use Symfony\Component\HttpKernel\Exception\HttpException;

class GenerateImageHelper
{


    /**
     * @param integer $docData
     *
     * @param array $fileConfigs
     *
     * @return String
    */
    public static function generateImage($docData, $fileConfigs, $fileExtension)
    {
        try
        {

            $baseDoc        = Image::make( $docData );

            $maxWidth       = $fileConfigs['max_width'];

            $maxHeight      = $fileConfigs['max_height'];

            $imageWidth     = $baseDoc->width();

            $imageHeight    = $baseDoc->height();

            // check imageWidth
            if ($imageWidth > $maxWidth)
            {
                $baseDoc = $baseDoc->resize($maxWidth, null, function ($constraint)
                {
                    $constraint->aspectRatio();
                });

                $imageHeight = $baseDoc->height();
            }

            // check imageHeight
            if ($imageHeight > $maxHeight)
            {
                $baseDoc = $baseDoc->resize(null, $maxHeight, function ($constraint)
                {
                    $constraint->aspectRatio();
                });

                $imageHeight = $maxHeight;
            }

            $baseDoc   = $baseDoc->encode($fileExtension);

            $uploadata = $baseDoc->getEncoded();

            return $uploadata;
        }
        catch(\Exception $e)
        {
            \Log::info(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " Exception ".print_r($e->getMessage(), true));

            throw new HttpException(HttpStatusCodesConsts::HTTP_BAD_REQUEST,"error while creating image");

        }
    }

}
