<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kirit
 * Date: 06/02/2020
 * Time: 04:39 PM
 */

namespace App\Http\Helpers;

class NetworkRequests
{

    public static function postRequestParams($api, $body, $xJwt)
    {


        $request = new \GuzzleHttp\Client();


            $response = $request->post($api, [
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded', 'X-JWT-Assertion' => $xJwt],
                'form_params' => $body
            ]);

        return $response;

    }

    public static function getRequestParams( $url, $xJwt )
    {

        $request  = new \GuzzleHttp\Client();

        $response = $request->get( $url, ['headers' =>['X-JWT-Assertion' => $xJwt] ] );

        return $response;

    }

    public static function postRequest($api, $body, $headers)
    {


        $request = new \GuzzleHttp\Client();


        $response = $request->post($api, [
            'headers' => $headers,
            'form_params' => $body
        ]);

        return $response;

    }

}
