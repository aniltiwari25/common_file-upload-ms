<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kirit Bellubbi
 * Date: 25/04/2018
 * Time: 10:36 AM
 */

namespace App\Http\Helpers;

class HtmlToPdfHelper
{
    /**
     * returns stream of PDF
     *
     * @param  $templateName
     * @param  $replaceParamsArr
     *
     * @return String
     *
    */
    public static function getPdfStream($templateName, $replaceParamsArr)
    {

        $htmlStr                = self::getHtmlTemplate( $templateName, $replaceParamsArr );

        $pdfStream              = self::generatePdfStream( $htmlStr );

        return $pdfStream;
    }

    /**
     * returns Html String
     *
     * @param  $templateName
     *
     * @return String
     *
     * @throws Exception
    */
    public static function getHtmlTemplate( $templateName, $replaceParamsArr )
    {

        $viewTemplate           = 'pdfTemplates.'.$templateName;

        if(! view()->exists( $viewTemplate ))
        {
            throw new \Exception(" View template does not exist ");

        }

        $htmlStr = view( $viewTemplate )->with('replaceParamsArr', $replaceParamsArr)->render();

        return $htmlStr;
    }

    /**
    * generatePdfStream
    *
    *
    * @param  $htmlStr
    *
    * @return String
    *
    */
    public static function generatePdfStream( $htmlStr  )
    {
        $pdfHandler             = \PDF::loadHTML($htmlStr);
        $pdfHandler->setOption('defaultFont','times-roman');
        $pdfHandler->setOption('fontHeightRatio',0.90);
        $pdfHandler->setOption('dpi',200);
        $pdfHandler->setOption('isHtml5ParserEnabled',true);

        $pdfStream              = $pdfHandler->stream();

        return $pdfStream;
    }

}
