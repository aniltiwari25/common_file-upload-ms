<?php
/**
 * Created by IntelliJ IDEA.
 * User: Kirit
 * Date: 12/03/2018
 * Time: 9:35 AM
 */

namespace App\Http\Helpers;

use Intervention\Image\Facades\Image;
use App\Constants\HttpStatusCodesConsts;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AWS_S3Helper
{


    /**
     * @param integer $customerId
     *
     * @param string $fileName
     *
     * @param object $uploadata
     *
     * @param Array metaData
     *
     * @param string $folderName
     *
     * @return string
     *
     * @throws Excepttion
    */
    public function uploadToS3($customerId, $fileName, $uploadata, $fileConfigs,  $folderName, $expire)
    {

        $s3url          = $folderName.'/'.$customerId.'/'.$fileName;

        $clientObj      = $this->getS3Driver( $fileConfigs );

        try
        {
            $clientObj->put( $s3url, $uploadata );

            return  $s3url;

        }
        catch(\Exception $e)
        {
            \Log::info( __FUNCTION__.'Exception Message '.print_r($e->getMessage() ,true ) );

            throw new HttpException( HttpStatusCodesConsts::HTTP_INTERNAL_SERVER_ERROR, " error while uploading to s3 ");

        }
    }

    /**
     * getDocuments
     *
     * @param Array $fileConfigs
     *
     * @param string $s3FilePath
     *
     * @return String
    */
    public function getFileContents($fileConfigs, $s3FilePath)
    {
        $s3Obj      = $this->getS3Driver($fileConfigs);

        $imageData  = $s3Obj->get( $s3FilePath );

        return $imageData;
    }

    /**
     * getPreSignedUrl
     *
     * @param Array $fileConfigs
     *
     * @param string $s3FilePath
     *
     * @return String
    */
    public function getPreSignedUrl($fileConfigs , $s3FilePath)
    {
        $s3Obj      = $this->getS3Driver($fileConfigs);
        $expiry     = env('SIGNED_URL_EXPIRY_IN_MINUTES', "+10 minutes");
        return $s3Obj->temporaryUrl($s3FilePath, $expiry);
        /*$clientObj  = $s3Obj->getDriver()->getAdapter()->getClient();



        $bucket     = $fileConfigs['bucket'];

        $command    = $clientObj->getCommand('GetObject', [
                        'Bucket' => $bucket,
                        'Key'    => $s3FilePath
        ]);

        $request = $clientObj->createPresignedRequest($command, $expiry);

        return (string) $request->getUri();
        */
    }

    /**
     * getS3Driver
     *
     * @param array $fileConfigs
     *
     * @return Object
    */
    public function getS3Driver(array $fileConfigs): object
    {
        //print_r($fileConfigs);
        $objS3 = \Storage::createS3Driver([
            'driver' => 's3',
            'key'    => $fileConfigs['key'],
            'secret' => $fileConfigs['secret'],
            'region' => $fileConfigs['region'],
            'bucket' => $fileConfigs['bucket'],
        ]);

        return $objS3;
    }



}
