<?php
/**
 * Created by IntelliJ IDEA.
 * User: ranveer
 * Date: 26/03/2017
 * Time: 4:39 PM
 */

namespace App\Http\Helpers;

use App\Http\Helpers\SThreeHelper;

/**
 * Class ImageHelper
 *
 * @package app\Http\Helpers
 * 
 */
class ImageHelper
{

    /**
     * create a Image   
     * @param $imageData
     * 
     * @return String
    */
    public static function createImage( $imageData ) 
    {

        $image              = \Image::make($imageData);
        $mime               = $image->mime();

        return $image->encode();

    }

}