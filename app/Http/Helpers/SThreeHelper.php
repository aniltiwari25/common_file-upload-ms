<?php
/**
 * Created by IntelliJ IDEA.
 * User: ranveer
 * Date: 04/05/2017
 * Time: 11:58 AM
 */

namespace App\Http\Helpers;



use App\Constants\HttpStatusCodesConsts;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SThreeHelper
{

    /**
     * @param      $xJwt
     * @param      $customerId
     * @param      $docTypeId
     * @param      $subDocTypeId
     * @param      $fileExtension
     * @param      $docData
     * @param      $fileName
     * @param      $folderName
     * @param      $expireDoc
     *
     * @throws Exception
     *
     * @return Array
     *
    */
    public static function uploadToS3( $xJwt,$customerId, $docTypeId, $subDocTypeId, $fileExtension, $docData, $fileName, $fileDesc, $folderName, $s3bucketDetails, $expireDoc)
    {

        $fileConfigs    = [
                                'driver'        => 's3',
                                'max_width'     => 1200,
                                'max_height'    => 1600,
                        ];

        $fileConfigs  = array_merge($fileConfigs,$s3bucketDetails);
        $uploadData      = $docData;

        $fileName = empty($fileDesc) ? $fileName : $fileDesc;

        $objAwsS3 = new AWS_S3Helper();

        $s3Url = $objAwsS3->uploadToS3($customerId, $fileName,$uploadData,$fileConfigs,  $folderName, $expireDoc);

        \Log::debug(__CLASS__ . " " . __FUNCTION__ . " " . __LINE__ . " S3 Url ".$s3Url);

        return ['s3Url' => $s3Url];

    }


    /**
     * @param  $s3Path
     *
     * @throws Exception
     *
     * @return Array
     *
    */
     public static function fetchS3Files( $s3Path, $signedUrl, $s3bucketDetails )
    {

        $objAwsS3 = new AWS_S3Helper();

        if($signedUrl)
        {
            $signedUrl = $objAwsS3->getPreSignedUrl($s3bucketDetails,$s3Path);
            $responseArr =  ['imageData' => $signedUrl ];
        }
        else
        {
            $imageData      = $objAwsS3->getFileContents($s3bucketDetails, $s3Path);

            $responseArr =  ['imageData' => $imageData ];
        }

        return $responseArr;
    }

}
