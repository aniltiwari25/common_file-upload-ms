<?php

namespace App\Exceptions;


class CustomException extends \Exception
{
    protected $message;
    protected $HttpResponseCode;
    protected $errorCode;

    /**
     * @param $message
     * @param $HttpResponseCode
     * @param $errorCode
     */
    public function __construct($message, $HttpResponseCode, $errorCode)
    {
        $this->message = $message;
        $this->HttpResponseCode = $HttpResponseCode;
        $this->errorCode = $errorCode;
    }


    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        \Log::error(__CLASS__." ".__FUNCTION__. " " . __LINE__ ." Error Message ". $this->message." Error Code ". $this->errorCode );
        return response( ['errorMessage' => $this->message, 'error' => $this->errorCode ], $this->HttpResponseCode);
    }
}
