<?php

namespace App\Constants;

class ErrorResponse
{
    const INVALID_CLIENT_ID = "Invalid Client Id";
    const INVALID_DOCTYPE_ID = "Invalid Doc TypeId";
    const INVALID_SUB_DOCTYPE_ID = "Invalid Sub Doc TypeId";
    const FILE_NOT_UPLOADED_MSG = "Upload File is mandatory in Request";
    const INVALID_DETAILS = "Invalid details";
}
