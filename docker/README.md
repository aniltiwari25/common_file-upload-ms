# AgentComissionMssqlApi Docker Image

This docker image is based on [kahatie/debian-lamp](https://hub.docker.com/r/kahatie/debian-lamp/)'s LAMP Stack docker container freely available on the [docker hub](https://hub.docker.com).

More information regarding how we modify the base container can be found inside a file called `Dockerfile` residing in this folder.

## Getting Started

### Step 1: 

Install and configure [docker](https://www.docker.com/community-edition) on your local computer.

### Step 2: 

Build the docker image: 

```
# Usage: ./build.sh <production-image-name>, default: hello/agent-commission-mssql-api
./build.sh 
```

### Step 3: 

Start the container and mount the `agent-commission-mssql-api` repository to the container if you want to use it for development purposes.

The format required to mount a local folder on your host computer to a folder residing inside the docker container with the `-v` flag is: `PATH_ON_YOUR_COMPUTER`:`REMOTE_PATH_INSIDE_DOCKER_CONTAINER`.

* The agent-commission-mssql-api source code needs to be mounted to `/var/www/agent-comission-mssql-api`

The full command to mount both the `agent-commission-mssql-api`  into the docker container is:

```
docker run -d --name=AgentComissionMssqlApi -v ~/Development/agent-commission-mssql-api:/var/www/agent-commission-mssql-api -v /etc/localtime:/etc/localtime:ro -p 4430:443 hello/agent-commission-mssql-api
```
Explanation: 

* `--name` - Refers to the container name
* `-d` - Run container in background and print container ID. 
* `-v` - Bind mount a volume
* `-p` - Publish a container's port(s) to the host.  
	* `LOCALHOST_PORT_NUMBER_ON_YOUR_PC`:`CONTAINER_PORT`
	* Navigating to [https://localhost:4439](https://localhost:4430) will trigger port `443` inside the docker container serving your source code as mounted in the previous steps.
	* If you would like to mount to laravel log files then consider adding the following flag: ` -v /opt/agent-commission-mssql-api/qa/logs:/var/www/html/storage/logs`

## QA Environment

### Publishing a new build to QA

Publishing your Custom Docker Image on Docker Hub

Your next option is to publish the created Docker image on the Docker Hub Repository. To do so, you will need to create an account on the Docker Hub signup webpage where you will provide a name, password, and email address for your account. I should also point out that the Docker Hub service is free for public docker images. Once you have created your account, you can push the image that you have previously created, to make it available for others to use.

To do so, you will need the ID and the TAG of your `hgdockerza/agent-commission-mssql-api` image.

Run again the `docker images` command and note the ID and the TAG of your Docker image e.g. 602d96f6a0d8.

Now, with the following command, we will prepare our Docker Image for its journey to the outside world (the accountname part of the command is your account name on the Docker Hube profile page):

	docker tag 602d96f6a0d8 hgdockerza/agent-commission-mssql-api:latest-qa
	
Run the `docker images` command and verify your newly tagged image.

Next, use the `docker login` command to log into the Docker Hub from the command line.

Login to your Docker account

The format for the login command is:	

	docker login --username=yourhubusername 
	
When prompted, enter your password and press enter.
	
Push the image back to the DockerHub

Now you can push your image to the newly created repository:

	docker push hgdockerza/agent-commission-mssql-api:latest-qa
	
The above command can take a while to complete depending on your connection's upload bandwidth as it uploads quite a bit of data. Once it has completed, you can go to your profile on Docker Hub and check out your new image.

Downloading your Custom Image

If you want to pull your image from your Docker Hub repository you will need to first delete the original image from your local machine because Docker would refuse to pull from the hub as the local and the remote images are identical.

As you remember from the previous part, to remove a docker image, you must run the `docker rmi` command. You can use an ID or the name to remove an image:	

	docker rmi -f 602d96f6a0d8
	
Now that the image is deleted you can pull and load the image from your repository using the `docker run` command by including your account name from Docker Hub.

	docker pull hgdockerza/agent-commission-mssql-api:latest-qa
	
	docker run hgdockerza/agent-commission-mssql-api:latest-qa
	
Since we previously deleted the image and it was no longer available on our local system, Docker will download it and store it in the designated location.

## Troubleshooting

### SSH into the AgentComissionMssqlApi container

```
bash -c "clear && docker exec -it AgentComissionMssqlApi bash"
```

### Rebuilding the docker container

Firstly we need to ensure that the container is stopped and removed to avoid conflicts with the container name. 

```
docker kill AgentComissionMssqlApi
docker rm AgentComissionMssqlApi
```

Now we can rebuild it using the build script we used in [**step 2**](#step2). 

```
./buildDocker.sh
```

Alternatively there is a rebuild script also available in the project's root folder: 
```
./rebuildDocker.sh
```

Start it up again as per [**step 3**](#step3)

### Tailing Docker container logs

```
docker logs -f AgentComissionMssqlApi
```

### Cleanup old docker images

**Dangerous use at your own risk!**
```
docker rmi `docker images | grep '<none>' | awk '{ print $3; }'`
```