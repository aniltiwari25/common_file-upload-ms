<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Guardrisk1</title>
    <style>
        body{
            line-height: 43px;
        }
      .container {
        padding-top: 10px;
      }
      .h-div {
        width: 49%;
        border: 1px solid black;
        padding: 10px;
          float: left;
      }
      .h-div > p {
        margin: 0px;
      }
      .logo-image {
        width: 100%;
        text-align: right;
          float: right;
          vertical-align: middle;
      }
      .field-section1 {
        margin-bottom: 25px;
      }
      .field-section {
        margin-top: 20px;
      }
      .field-section2 {
        margin-top: 20px;
      }
      .field-section3 {
        border: 1px solid black;
      }
      .field-section3 > p {
        margin: 0px;
        padding: 10px;
        font-weight: 900;
      }
      table {
        width: 100%;
        table-layout: fixed;
        border: 1px solid black;
        border-collapse: collapse;
      }
      thead {
        font-weight: 900;
      }
      td {
        /* width: 100%; */
        padding: 5px;
        border: 1px solid black;
      }
      .text-center {
        text-align: center;
      }
      .footer {
        margin-top: 40px;
      }
      .pad-10 {
        padding: 10px;
      }
      .text-right {
        text-align: right;
      }
      .page-break {
        page-break-after: always;
      }
      ul{
          line-height: 32px;font-size: 26px;
      }
      .list ul li {
        list-style: none;
      }

      .text-normal {
        font-weight: normal !important;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="field-section1">
          <table style="border: 0px;">
              <tbody>
              <tr>
                  <td style="border: 1px solid black;">

                      <p>
                          <strong>Hello Protect (FSP 49260) Policy Schedule</strong><br>
                          Underwritten by <strong>Guardrisk Microinsurance Life Limited</strong>
                      </p>

                  </td>
                  <td style="text-align: right; width:50%; vertical-align: middle ; border:none">

                          <img
                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAAAtCAYAAACKyQbtAAAACXBIWXMAABCcAAAQnAEmzTo0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAtvSURBVHgB7Z1Ndtu6FccvlfSNmjy+WQdNgqwg8goqr+DJK4i8AtsrsLwCJyuwsgLLg47NrsDqCoyX157TIdvO2lTsvQAkURKBC5KgpBfhdw5jRSS+8QdA4AJKIP3jAKD3CHYk5F/fw6GTvhkBJHeOJ6rTwacf0F0CkYOiuIcRJOAq7ywZwil8p2D6Bab/2fUMpn+r3vYgEokcDVHwkcgREQUfiRwRUfCRyBERBR+JHBFR8JHIEREFH4kcES+hLmmaArwa6v8k+LmQAP+dQf4PCU1Y+fcBL0HflO7O8Por+p819n9fpEIAzE26ClG6k2O+YZrmmKa/ZS4vcK2V8iJ1PJInZ+Tf8vk+/umX7ku8ZuVn6oaBbuXG87XDKLkdKLeJKutFmDkUVMbKj8zDj0vlNoF3zKMCnx2Xv0D/x8D7T/EaYlf4AeO1ypcCfgFdH2ebedIEtY6uw3lXEU5mywt0R3WK8j8FPozxxleTxNvwRj2XXOM1sHifoTDPvYWphP76Aj/pAmQpJuj/jdX/QzG8YfNpI05QYJp+nVTdVAJJwBW3STKE8+IBLrCijMGejxne/4KVaFIRxgTD+GgNoVDGKzOsmBSGq6wmeP+mooFIPdwukBiXKczhs01UxVQZmwhoQJUhSimewhjyDIAnM2nNoAamMSGDoZ89wpGlcGTJD3d5OSMAp35D+vTttRaFqxLTvR+e9bOcfySK10/4aQxeYlf+j9D/J/T/Eg4RasDSt7d8Pq0hVCOVvsV8+4OA+qQogEcsyE/gzkdqOO6wYbiF+pAQnpgGhaCK/IxhLCujGg34uV2FRQ0DNnJmNLATVIOprdYGnk5UQ0z5aUTMh6Eb7ye8bsEvHAE6Tx/NqCYIPoIXAPxQqMTYKcr07UfTowqoD2XurVejskv0aIXS1LRgBDZmjw1ET8O7gffTKCZsIO6gDrrXE97Pz0GNopTw9ejE3+0KYQTVrCergcoP3WA2cLxsnJyix3Rct8yL21Ci72rS7lqLYANdoSfQnvFh9fSvqNXuQzuEFn3qOeJpzEjZoXeBHn6O1fCYH3X4+DfpsqdXQoTWeUGjmHtnGEWtDrMKmh/JIABdCZ56vApB/vAI4bhuOAwOi547GEEYRHW+BSbpoOfUcwRj4z+Vc5iGi15F7iF4I6gavfZCXDCo6oFNwzeGdtCk5inm7QwC0OWy3HqlUsJoNtliASvB7w5gaJ+EjsPFDnr5QfFndpbbHy32kfqoRw8CwiFg/VVJmotbFchLzy6uFQmELTf0b6thah9GldjL6eKQG1enghfrFZcVBiaqOMco4Ttg70R/5hKFPWv34rCjJh+9KrfEuGYAXq00jY5G0DX/CbR1tCR2hf/oQYJfpSU//7T8OEQBDHGeoIArxtWMnitfixuejdJErVAUKiz6+4V5nurhZSkMPSPvh4SqvCjgfLNnx/9fmvSz5beZflpVqCP4GxTiT/ryESPxaqD+8MKQ6O+pWp7KJS6fyZleqiLhc+HsQBxWekOPh27UcmD+C6bvK6WHq6jEB6hDAZ/xOjGXZ9nUmnOQptL/ZMK4MeGui10z8PFrKcJCCZFrCAdBh/V6WcwONia03EkCoSUx83e0TLfd33JjNwCe7bxYNCxa7FMIjK/gr7CyjlGIub5IjPNzD3emkDhh0Fq03B6iqe8KdyZDd5M6PAkjzCJT+VYm/0qTWZ/d7mqkKYFPqtU/UwYhM7XWXnj13gJ80cNKqvS5CWNMlXNT7F4TbLoiZ4v/qjXmAs6Ap+2kaJmB4x4JvHLW3sxRuF4lROlVaQAcJl9L/kvTsJxU2UyEwE/wupJufKesxDwnEjhh/NvRkr3gWrl6vWFQCqbXSR4s7jJwI8CX+XbjYQw1Mqe7BH4EP7IqIxiLYQzXC+dVxirGL64uCQiAsRS0x7OAB7cHzNB+8aqUsPVyYjMuCjVBV4WPaa2030qoBwaeNdPSCl7j2vpry705MAjYH8J9m8yOq3gxY9OVvsee4hkYcquZJ5mrJgFGPwX8pcbTgrkvwR4Oxbffwm9fhPMuzhcwtgrcSEOYv+7GT5sT75z6tvTNEMz9EbRBieMb7AGmRyu4meS2uPyXsHvYHh6akgRbVRDM/c29Ak3h8qKzXtxF3C23D2hiMgzSca/rxiY0EnbDrlZ19rd65CAK/ng5yAr5HREFHzGk4hAqw6FVyO+tAZJwgOzqHZ6Gl64ClfDbhElXYrsXqnKLhve6gnuNSBveo0muf0IYJHM/h1U6BHSHgD1wIIL/1wnkebt3zvQN7AFO8KL6+2+CHVzlz3QQwnv4bdFc8PykXKg5Cbc/2pjoEroOZ0+C39WQnlna+b3frOg+zWir4QrVshbb49LrW7mF1QKtZI7aMow6cDPPwmExJ6Cd3764/UkC2XVwy25J91t/q9iV4JnC6t2xO9/Stxfq0IxD2CG3gltLHW7FV///wu2sqFO5q3dp8ZZeXQhesk/0tjeUqAMoeMEHia+xW3D5NeD2nlP+4lr9E7Nfn2/8Hiry4h766Pd9FzsEiR0N6f+XAbxwPSD0XvA328c9LY+MWlRgtWe8yStAqsS2dUSWx1o5xaH6/Dkq1I/OMHW6pqAah4SGrSPgDXb8jTISuMDKIfHTlExfzckq/Mk24d6JV1Gh8KcqLgLs4dIhHH1j0Zarnq7gDYTqHieF9EmYFsMkKreB1SUdOPEAP8JcDe+X7ktHVFF9TNV+fUxF8nOl9V0GHAWMMS/E0rhJj8pG5jONhk59zgq0eo91YTPfdiP4/O8zSN9lGIWB4ylhjnuiyrpIZArb731CnS6Twum66MmqzXn0XGqO4JKrr3pkwy15a77e47o7+KzNjXsTdKsL347AeNV8J3w5qfFwak6luSumUIdQQ+R1yPSU3xY6qGkFOKn4jp8vSLAXnq6eW+6Yo8aGC5/2sSdKkLL0rah4rlL0ZB6LbjPgR1rUgIwqvu+b03RsoucbAnJfjn8BZztclptzG0YWkHgEbJ9gW6avRV9+p39JFdinNRSr61tqNu3Ieu5MvLTbLxAU2nAjuxHjOhl0AzaEgV8XqnepZcBTrkui9P0EasyTwLb7dbToP1Z837Zu9MHyC7mmEfCpJ6J0pbsTfP7r1OwJD8W66JX4GvvfomB6Ywi6rPjCZxdiW2SDIbIXqiJy20jrUFSfYGsqfAYNCB5HokL0ZsdbBu0YWm37uY0+FezY8Cbx3avty8P6sD5pWIi95r2SamjUq0GIXu0qoNmtnSL0qGQdtb00TBjUg42td1uI1sQxpOglvhlur0b5n0/gYmQ5cfhTXb93K3hVmXu0fVBCO3J1CMfWXnM1FPY5YGIzXujf3GdPts39rGW6Fun5BN0zS86CneVmxezr9n2NqyIz+8VzRxhZS9GPA4k+M3vYZUUYcnm2f3OkZRt0bhoUb3ZvWqtOtFE/CNEwo2kvee/E9uMNRjT1/Vaz8IWZxGvASvQ1ezYmPUyoUG/ImHkejhEEZcBSv4fLzYkzXjPUbUW7OMyj4YjEK67qYIthrdOIyv7f2BoT43dmDhCRPh7uz5Ze9c50fh0dl5VMwT4kXkxOYKHO6RisU3bYu/RbFaL72TV3NM+gRHsFTVpk3ZiNPMKmNE280+MIkSqbR0XK1Ekzw3bLPE2g91hzfBPFkcpZVjyWmzhemZN0ao10NkQroSalk2bem1FJ5nhcgj7v7rxuXCvywjb7nhmhk/9jrszUUVirc/ec9TZR/6rfQXNgq5B6E0jqcll5dJWLzbiEeKe1x9Mdv6bu3H54u/X4qSm5cTjjANZ/d0yC5cSakhu2DEM3Ehthdu3/kqTmb8IZA6Yynce1bhx9/CyRJxA5WOoKPhLhiNtjI5EjIgo+EjkiouAjkSMiCj4SOSKi4CORIyIKPhI5IqLgI5Ej4v8GLGGURnwp3AAAAABJRU5ErkJggg=="
                          />

                  </td>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="field-section">
        <table>
          <tbody>
            <tr>
              <td><strong>Policy Number</strong></td>
              <td id="PolicyNumber"> @isset($replaceParamsArr['policyNumber']) {{ $replaceParamsArr['policyNumber'] }}@endisset</td>
              <td><strong>Inception Date</strong></td>
              <td id="CoverStartDate">@isset($replaceParamsArr['inceptionDate']){{ $replaceParamsArr['inceptionDate'] }}@endisset</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><strong>End Date</strong></td>
              <td id="coverEndDate">@isset($replaceParamsArr['endDate']) {{$replaceParamsArr['endDate'] }}@endisset </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="field-section">
          <table>
              <tbody>
              <tr>
                  <td><strong>Insured</strong></td>
                  <td id="insured" colspan="3">@isset($replaceParamsArr['customerFirstName']) {{$replaceParamsArr['customerFirstName'] }}@endisset  @isset($replaceParamsArr['customerSurname'])  {{$replaceParamsArr['customerSurname'] }}@endisset</td>
              </tr>
              <tr>
                  <td><strong>Cell Number</strong></td>
                  <td id="cellNumber" colspan="3">@isset($replaceParamsArr['customerMsisdn'])  {{$replaceParamsArr['customerMsisdn'] }}@endisset</td>

              </tr>
              <tr>
                  <td><strong>Email Address</strong></td>
                  <td id="insured" colspan="3">@isset($replaceParamsArr['customerEmail'])  {{$replaceParamsArr['customerEmail'] }} @endisset </td>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="field-section">
          <table>
              <tbody>
              <tr>
                  <td><strong>Policy Administrator</strong></td>
                  <td id="policyBrocker">@isset($replaceParamsArr['policyBroker']) {{$replaceParamsArr['policyBroker'] }} @endisset</td>
                  <td><strong>Telephone</strong></td>
                  <td id="telephone">@isset($replaceParamsArr['brokerTelephone']) {{$replaceParamsArr['brokerTelephone'] }} @endisset</td>
              </tr>
              <tr>
                  <td><strong>Intermediary</strong></td>
                  <td colspan="3" id="intermediary">@isset($replaceParamsArr['intermediaryName']) {{$replaceParamsArr['intermediaryName'] }} @endisset</td>
              </tr>
              <tr>
                  <td><strong>Email</strong></td>
                  <td  colspan="3">@isset($replaceParamsArr['brokerEmail']) {{$replaceParamsArr['brokerEmail'] }} @endisset</td>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="field-section">
        <table>
          <tr>
            <td><strong>Item</strong></td>
            <td><strong>Nett</strong></td>
            <td><strong>Vat</strong></td>
            <td><strong>Total</strong></td>
          </tr>
          <tr>
            <td><strong>Policy Premium</strong></td>
            <td id="policyPremiumNett">@isset($replaceParamsArr['policyPremiumNet']) R {{$replaceParamsArr['policyPremiumNet'] }} @endisset</td>
            <td id="policyPremiumVat">@isset($replaceParamsArr['policyPremiumVat']) R  {{$replaceParamsArr['policyPremiumVat'] }} @endisset</td>
            <td id="policyPremiumTotal">@isset($replaceParamsArr['policyPremiumTotal']) R {{$replaceParamsArr['policyPremiumTotal'] }} @endisset</td>
          </tr>
          <tr>
            <td><strong>Total</strong></td>
            <td id="totalNett">@isset($replaceParamsArr['policyTotalPremiumNet']) R {{$replaceParamsArr['policyTotalPremiumNet'] }} @endisset</td>
            <td id="totalVat"> @isset($replaceParamsArr['policyTotalPremiumVat']) R  {{$replaceParamsArr['policyTotalPremiumVat'] }} @endisset</td>
            <td id="totalTotal">@isset($replaceParamsArr['policyTotalPremiumTotal']) R  {{$replaceParamsArr['policyTotalPremiumTotal'] }}@endisset</td>
          </tr>
        </table>
      </div>
      <div class="field-section">
        <table>
          <tr>
            <td><strong>Cover Amount</strong></td>
            <td class="text-right" colspan="4"> @isset($replaceParamsArr['policyCoverAmount']) R {{ $replaceParamsArr['policyCoverAmount']  }} @endisset</td>
          </tr>
        </table>
      </div>
      <div class="field-section2">
        <div class="field-section3">
          <p>1. INTRODUCTION</p>
          <ul>
            <li>
              1.1 You have voluntarily applied for this Accidental Death Policy.
              This Policy is optional and has been taken out because it meets
              your financial needs.
            </li>
          </ul>
        </div>
        <div class="field-section3">
          <p>2. DETAILS AND DECRIPTION OF THE TRANSACTION</p>
            <ul>
            <li>
              2.1 Your Intermediary, an authorised Financial Services Provider, is
                @isset($replaceParamsArr['intermediaryName']) {{$replaceParamsArr['intermediaryName']  }} @endisset with FSP number
                @isset($replaceParamsArr['intermediaryFSPNumber']) {{$replaceParamsArr['intermediaryFSPNumber']  }} @endisset. Your
              Administrator @isset($replaceParamsArr['policyBroker']) {{$replaceParamsArr['policyBroker'] }}, @endisset  an authorised Financial Services
              Provider, with FSP number 49260.
            </li>
            <li>
              2.2 @isset($replaceParamsArr['intermediaryName']) {{$replaceParamsArr['intermediaryName']  }} @endisset provides an intermediary service
              in terms of the Financial Advisory and Intermediary Services Act
              37 of 2002 (hereinafter refer to as ‘’FAIS’’).
            </li>
            <li>2.3 This schedule contains proof of cover.</li>
            <li>
              2.4 You can log into your personal profile at any time to manage
              this Policy. Alternatively, you can contact the call centre on
                @isset($replaceParamsArr['callCenterNo'])  {{$replaceParamsArr['callCenterNo']  }} @endisset   or
                @isset($replaceParamsArr['callCenterEmail'])  {{$replaceParamsArr['callCenterEmail']  }} @endisset   should You require further
              information or advice relating to this product.
            </li>
            <li>
              2.5 Guardrisk Microinsurance Limited is contactable on Tel: 011
              669 1000 ; Email: info@guardrisk.co.za / website:
              www.guardrisk.co.za / Complaints: complaints@guardrisk.co.za
            </li>
            <li>
              2.6 Premiums are guaranteed for the first 12 (twelve) months from
              the Entry Date and are reviewable annually. Any change to the
              premium will be notified to the policyholder 31 (thirty-one) days
              before any increase takes effect confirming the reason for
              increase.
            </li>
            <li>
              2.7 Failure to pay your monthly premium will result in a grace
              period of 31 (thirty-one) days during which the premium must be
              paid. Failure to pay 2 (two) consecutive premiums will result in
              the Policy coming out of force (by way of a lapse) and all
              benefits will cease and no cover shall thereafter remain in force.
              Should a claim event occur during the grace period, the benefit
              will still be paid less any arrear premium.
            </li>
            <li>
              2.8 Should you need to complain, you must contact the
              Administrator first, If you remain dissatisfied with the
              Administrator’s response or handling of your complaint, you can
              escalate a complaint to Guardrisk Microinsurance Limited (details
              provided under 2.6). Should you still remain dissatisfied with the
              Insurer’s response, you can then escalate a complaint to the
              office of the Long Term Insurance Ombudsman (for claims /service
              related matters) on Email: info@ombud.co.za / Tel: 021 657 5000 or
              the office of the FAIS Ombud for (advice related matters) on
              Email: info@faisombud.co.za / Tel: 012 762 5000 or the Information
              Regulator (for any personal information breach) on Email:
              complaints.IR@justice.gov.za / Tel: 010 023 5200
            </li>
          </ul>
        </div>
      </div>

      <div class="field-section">
        <table>
          <tbody>
            <tr>
              <td class="pad-10">
                Signed by @isset($replaceParamsArr['managingDirector'])  {{$replaceParamsArr['managingDirector']  }} @endisset <br> Managing Director <br>
                  @isset($replaceParamsArr['policyBroker'])  {{$replaceParamsArr['policyBroker']  }} @endisset
              </td>
              <td class="pad-10">&nbsp; @isset($replaceParamsArr['managingDirector'])  {{$replaceParamsArr['managingDirector']  }} @endisset  </td>
              <td class="pad-10"><strong>Date</strong></td>
              <td class="pad-10" id="createDate">@isset($replaceParamsArr['createdDate'])  {{$replaceParamsArr['createdDate']  }} @endisset</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div id="footer" class="footer">
          <table  style="border: none;">
              <tbody>
              <tr>
                  <td style="text-align: left; border: none;">Underwritten by @isset($replaceParamsArr['underwriterName'])  {{$replaceParamsArr['underwriterName']  }} @endisset </td>
                  <td style="text-align: right; border: none;">Printed on:
                      @isset($replaceParamsArr['year'])  {{$replaceParamsArr['year']  }} @endisset -
                      @isset($replaceParamsArr['month']) {{$replaceParamsArr['month']  }} @endisset -
                      @isset($replaceParamsArr['day'])  {{$replaceParamsArr['day']  }} @endisset
                  </td>
              </tr>
              </tbody>
          </table>

      </div>
    </div>

    <div class="page-break"></div>

    <div class="container">
      <div class="field-section1">
          <table style="border-width: 0px;">
              <tbody>
              <tr>
                  <td style="border-width: 1px;">

                      <p>
                          <strong>Hello Protect (FSP 49260) Policy Schedule</strong><br>
                          Underwritten by <strong>Guardrisk Microinsurance Life Limited</strong>
                      </p>

                  </td>
                  <td style="text-align: right; width:50%; vertical-align: middle; border: none ">

                          <img
                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPwAAAAtCAYAAACKyQbtAAAACXBIWXMAABCcAAAQnAEmzTo0AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAtvSURBVHgB7Z1Ndtu6FccvlfSNmjy+WQdNgqwg8goqr+DJK4i8AtsrsLwCJyuwsgLLg47NrsDqCoyX157TIdvO2lTsvQAkURKBC5KgpBfhdw5jRSS+8QdA4AJKIP3jAKD3CHYk5F/fw6GTvhkBJHeOJ6rTwacf0F0CkYOiuIcRJOAq7ywZwil8p2D6Bab/2fUMpn+r3vYgEokcDVHwkcgREQUfiRwRUfCRyBERBR+JHBFR8JHIEREFH4kcES+hLmmaArwa6v8k+LmQAP+dQf4PCU1Y+fcBL0HflO7O8Por+p819n9fpEIAzE26ClG6k2O+YZrmmKa/ZS4vcK2V8iJ1PJInZ+Tf8vk+/umX7ku8ZuVn6oaBbuXG87XDKLkdKLeJKutFmDkUVMbKj8zDj0vlNoF3zKMCnx2Xv0D/x8D7T/EaYlf4AeO1ypcCfgFdH2ebedIEtY6uw3lXEU5mywt0R3WK8j8FPozxxleTxNvwRj2XXOM1sHifoTDPvYWphP76Aj/pAmQpJuj/jdX/QzG8YfNpI05QYJp+nVTdVAJJwBW3STKE8+IBLrCijMGejxne/4KVaFIRxgTD+GgNoVDGKzOsmBSGq6wmeP+mooFIPdwukBiXKczhs01UxVQZmwhoQJUhSimewhjyDIAnM2nNoAamMSGDoZ89wpGlcGTJD3d5OSMAp35D+vTttRaFqxLTvR+e9bOcfySK10/4aQxeYlf+j9D/J/T/Eg4RasDSt7d8Pq0hVCOVvsV8+4OA+qQogEcsyE/gzkdqOO6wYbiF+pAQnpgGhaCK/IxhLCujGg34uV2FRQ0DNnJmNLATVIOprdYGnk5UQ0z5aUTMh6Eb7ye8bsEvHAE6Tx/NqCYIPoIXAPxQqMTYKcr07UfTowqoD2XurVejskv0aIXS1LRgBDZmjw1ET8O7gffTKCZsIO6gDrrXE97Pz0GNopTw9ejE3+0KYQTVrCergcoP3WA2cLxsnJyix3Rct8yL21Ci72rS7lqLYANdoSfQnvFh9fSvqNXuQzuEFn3qOeJpzEjZoXeBHn6O1fCYH3X4+DfpsqdXQoTWeUGjmHtnGEWtDrMKmh/JIABdCZ56vApB/vAI4bhuOAwOi547GEEYRHW+BSbpoOfUcwRj4z+Vc5iGi15F7iF4I6gavfZCXDCo6oFNwzeGdtCk5inm7QwC0OWy3HqlUsJoNtliASvB7w5gaJ+EjsPFDnr5QfFndpbbHy32kfqoRw8CwiFg/VVJmotbFchLzy6uFQmELTf0b6thah9GldjL6eKQG1enghfrFZcVBiaqOMco4Ttg70R/5hKFPWv34rCjJh+9KrfEuGYAXq00jY5G0DX/CbR1tCR2hf/oQYJfpSU//7T8OEQBDHGeoIArxtWMnitfixuejdJErVAUKiz6+4V5nurhZSkMPSPvh4SqvCjgfLNnx/9fmvSz5beZflpVqCP4GxTiT/ryESPxaqD+8MKQ6O+pWp7KJS6fyZleqiLhc+HsQBxWekOPh27UcmD+C6bvK6WHq6jEB6hDAZ/xOjGXZ9nUmnOQptL/ZMK4MeGui10z8PFrKcJCCZFrCAdBh/V6WcwONia03EkCoSUx83e0TLfd33JjNwCe7bxYNCxa7FMIjK/gr7CyjlGIub5IjPNzD3emkDhh0Fq03B6iqe8KdyZDd5M6PAkjzCJT+VYm/0qTWZ/d7mqkKYFPqtU/UwYhM7XWXnj13gJ80cNKqvS5CWNMlXNT7F4TbLoiZ4v/qjXmAs6Ap+2kaJmB4x4JvHLW3sxRuF4lROlVaQAcJl9L/kvTsJxU2UyEwE/wupJufKesxDwnEjhh/NvRkr3gWrl6vWFQCqbXSR4s7jJwI8CX+XbjYQw1Mqe7BH4EP7IqIxiLYQzXC+dVxirGL64uCQiAsRS0x7OAB7cHzNB+8aqUsPVyYjMuCjVBV4WPaa2030qoBwaeNdPSCl7j2vpry705MAjYH8J9m8yOq3gxY9OVvsee4hkYcquZJ5mrJgFGPwX8pcbTgrkvwR4Oxbffwm9fhPMuzhcwtgrcSEOYv+7GT5sT75z6tvTNEMz9EbRBieMb7AGmRyu4meS2uPyXsHvYHh6akgRbVRDM/c29Ak3h8qKzXtxF3C23D2hiMgzSca/rxiY0EnbDrlZ19rd65CAK/ng5yAr5HREFHzGk4hAqw6FVyO+tAZJwgOzqHZ6Gl64ClfDbhElXYrsXqnKLhve6gnuNSBveo0muf0IYJHM/h1U6BHSHgD1wIIL/1wnkebt3zvQN7AFO8KL6+2+CHVzlz3QQwnv4bdFc8PykXKg5Cbc/2pjoEroOZ0+C39WQnlna+b3frOg+zWir4QrVshbb49LrW7mF1QKtZI7aMow6cDPPwmExJ6Cd3764/UkC2XVwy25J91t/q9iV4JnC6t2xO9/Stxfq0IxD2CG3gltLHW7FV///wu2sqFO5q3dp8ZZeXQhesk/0tjeUqAMoeMEHia+xW3D5NeD2nlP+4lr9E7Nfn2/8Hiry4h766Pd9FzsEiR0N6f+XAbxwPSD0XvA328c9LY+MWlRgtWe8yStAqsS2dUSWx1o5xaH6/Dkq1I/OMHW6pqAah4SGrSPgDXb8jTISuMDKIfHTlExfzckq/Mk24d6JV1Gh8KcqLgLs4dIhHH1j0Zarnq7gDYTqHieF9EmYFsMkKreB1SUdOPEAP8JcDe+X7ktHVFF9TNV+fUxF8nOl9V0GHAWMMS/E0rhJj8pG5jONhk59zgq0eo91YTPfdiP4/O8zSN9lGIWB4ylhjnuiyrpIZArb731CnS6Twum66MmqzXn0XGqO4JKrr3pkwy15a77e47o7+KzNjXsTdKsL347AeNV8J3w5qfFwak6luSumUIdQQ+R1yPSU3xY6qGkFOKn4jp8vSLAXnq6eW+6Yo8aGC5/2sSdKkLL0rah4rlL0ZB6LbjPgR1rUgIwqvu+b03RsoucbAnJfjn8BZztclptzG0YWkHgEbJ9gW6avRV9+p39JFdinNRSr61tqNu3Ieu5MvLTbLxAU2nAjuxHjOhl0AzaEgV8XqnepZcBTrkui9P0EasyTwLb7dbToP1Z837Zu9MHyC7mmEfCpJ6J0pbsTfP7r1OwJD8W66JX4GvvfomB6Ywi6rPjCZxdiW2SDIbIXqiJy20jrUFSfYGsqfAYNCB5HokL0ZsdbBu0YWm37uY0+FezY8Cbx3avty8P6sD5pWIi95r2SamjUq0GIXu0qoNmtnSL0qGQdtb00TBjUg42td1uI1sQxpOglvhlur0b5n0/gYmQ5cfhTXb93K3hVmXu0fVBCO3J1CMfWXnM1FPY5YGIzXujf3GdPts39rGW6Fun5BN0zS86CneVmxezr9n2NqyIz+8VzRxhZS9GPA4k+M3vYZUUYcnm2f3OkZRt0bhoUb3ZvWqtOtFE/CNEwo2kvee/E9uMNRjT1/Vaz8IWZxGvASvQ1ezYmPUyoUG/ImHkejhEEZcBSv4fLzYkzXjPUbUW7OMyj4YjEK67qYIthrdOIyv7f2BoT43dmDhCRPh7uz5Ze9c50fh0dl5VMwT4kXkxOYKHO6RisU3bYu/RbFaL72TV3NM+gRHsFTVpk3ZiNPMKmNE280+MIkSqbR0XK1Ekzw3bLPE2g91hzfBPFkcpZVjyWmzhemZN0ao10NkQroSalk2bem1FJ5nhcgj7v7rxuXCvywjb7nhmhk/9jrszUUVirc/ec9TZR/6rfQXNgq5B6E0jqcll5dJWLzbiEeKe1x9Mdv6bu3H54u/X4qSm5cTjjANZ/d0yC5cSakhu2DEM3Ehthdu3/kqTmb8IZA6Yynce1bhx9/CyRJxA5WOoKPhLhiNtjI5EjIgo+EjkiouAjkSMiCj4SOSKi4CORIyIKPhI5IqLgI5Ej4v8GLGGURnwp3AAAAABJRU5ErkJggg=="
                          />

                  </td>
              </tr>
              </tbody>
          </table>
      </div>
      <div class="field-section">
        <table>
          <thead>
          <tr>
            <td>Personal Accident</td>
            <td>Description</td>
          </tr>
          </thead>
          <tbody>
          <tr>
            <td>Insured Name</td>
            <td id="insuredName">@isset($replaceParamsArr['customerFirstName'])   {{$replaceParamsArr['customerFirstName']  }} @endisset</td>
          </tr>
          <tr>
            <td>Insured Surname</td>
            <td id="insuredSurname">@isset($replaceParamsArr['customerSurname'])   {{$replaceParamsArr['customerSurname']  }} @endisset</td>
          </tr>
          <tr>
            <td>ID or Passport Number</td>
            <td id="idOrPassportNumber">@isset($replaceParamsArr['customerIdNumber'])   {{$replaceParamsArr['customerIdNumber']  }} @endisset</td>
          </tr>
          <tr>
            <td>Sum Insured</td>
            <td id="sumInsured">@isset($replaceParamsArr['policyCoverAmount']) R  {{$replaceParamsArr['policyCoverAmount']  }} @endisset</td>
          </tr>
          </tbody>
        </table>
      </div>
      <div class="field-section2">
        <div class="field-section3 list">
          <p>3. WAITING PERIODS AND EXCLUSIONS AND FEES</p>
          <p class="text-normal">
            You acknowledge, understand and confirm that the following has been
            taken into consideration with your application:
          </p>
          <ul>
            <li>3.1.1 this policy meets your financial needs;</li>
            <li>
              3.1.2 you understand the benefits applicable under this Policy;
            </li>
            <li>3.1.3 there is no waiting period for accidental death;</li>
            <li>
              3.1.4 there is a 12 (twelve) months waiting period for death as a
              result from suicide from the Entry Date;
            </li>
            <li>
              3.1.5 this Policy excludes the following:
              <ul>
                <li>
                  3.1.5.1 Intentionally self-inflicted injury, suicide or a
                  suicide attempt (whether sane or insane) within the <br />
                  first 12 (twelve) months from the Entry Date;
                </li>
                <li>3.1.5.2 The Insured breaching of any (criminal) law;</li>
                <li>
                  3.1.5.3 The Insured’s participation in any criminal
                  activities;
                </li>
                <li>
                  3.1.5.4 No payment will be made under this Policy if the
                  premiums have not been paid up to date or if the <br />Insured
                  has not complied with all the obligations and <br />conditions
                  of this Policy;
                </li>
                <li>
                  3.1.5.5 Nuclear accidents, radioactivity, war or armed
                  conflict (whether war be declared or not), terrorist or
                  insurgency activities, rebellion, civil commotion,
                  sedation,<br />
                  sabotage or any activity associated with the aforegoing or the
                  defense, quelling investigation or <br />
                  containment thereof by any security force;
                </li>
                <li>
                  3.1.5.6 Illegal acts of the Insured or the Insured’s
                  representatives will not be covered.
                </li>
              </ul>
            </li>
            <li>
              3.1.6 an intermediary fee of 3.25% and a binder fee of 9% is
              included in your monthly premium.
            </li>
          </ul>
        </div>
      </div>
      <div class="field-section2">
        <div class="field-section3">
          <p>PROCESSING OF PERSONAL INFORMATION</p>
          <p class="text-normal">
            Processing of Personal Information in terms of the Protection of
            Personal Information Act 4 of 2013 Your privacy is of utmost
            importance to Us. We will take the necessary measures to ensure that
            any and all information, provided by you for the purpose of this
            application, is processed in accordance with the provisions of the
            Protection of Personal Information Act 4 of 2013 and further, is
            stored in a safe and secure manner. You hereby agree to give honest,
            accurate and up-to-date Personal Information in order to process and
            accept this application.
          </p>
          <p class="text-normal">
            You accept that your Personal Information collected by Us may be
            used for the following reasons:
          </p>
          <ul>
            <li>
              to establish and verify your identity in terms of the Applicable
              Laws;
            </li>
            <li>to enable Us to issue and administer this Policy;</li>
          </ul>
          <p class="text-normal">
            Unless consented to by yourself, we will not sell, exchange,
            transfer, rent or otherwise make available your Personal Information
            (such as your name, address, email address, telephone or fax number)
            to any other parties and you indemnify Us from any claims resulting
            from disclosures made with your consent.
          </p>
          <p class="text-normal">
            You understand that if the Administrator/Guardrisk Microinsurance
            Limited has utilised your Personal Information contrary to the
            Applicable Laws, you have the right to lodge a complaint with
            Guardrisk Microinsurance Limited or with the Information Regulator
          </p>
        </div>
      </div>

      <div id="footer" class="footer">
          <table  style="border: none;">
              <tbody>
              <tr>
                  <td style="text-align: left; border: none;">Underwritten by @isset($replaceParamsArr['underwriterName'])  {{$replaceParamsArr['underwriterName']  }} @endisset </td>
                  <td style="text-align: right; border: none;">Printed on:
                      @isset($replaceParamsArr['year'])  {{$replaceParamsArr['year']  }} @endisset -
                      @isset($replaceParamsArr['month']) {{$replaceParamsArr['month']  }} @endisset -
                      @isset($replaceParamsArr['day'])  {{$replaceParamsArr['day']  }} @endisset
                  </td>
              </tr>
              </tbody>
          </table>
      </div>
    </div>
  </body>
</html>
