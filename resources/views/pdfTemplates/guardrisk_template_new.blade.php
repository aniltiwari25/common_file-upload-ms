<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Guardrisk1</title>
    <style>
        /*body{
           ## line-height: 43px;
        }*/
        .container {
            padding-top: 10px;
        }
        .h-div {
            width: 49%;
            border: 1px solid black;
            padding: 10px;
            float: left;
        }
        .h-div > p {
            margin: 0px;
        }
        .logo-image {
            width: 100%;
            text-align: center;
        }
        .field-section1 {
            margin-bottom: 15px;
        }
        .field-section {
            margin-top: 10px;
        }
        .field-section2 {
            margin-top: 10px;
        }
        .field-section3 {
            border: 1px solid black;
        }
        .field-section3 > p {
            margin: 0px;
            padding: 10px;
            font-weight: 800;
        }
        table {
            width: 100%;
            table-layout: fixed;
            border: 1px solid black;
            border-collapse: collapse;
        }
        thead {
            font-weight: 800;
        }
        td {
            /* width: 100%; */
            padding: 5px;
            border: 1px solid black;
        }
        .text-center {
            text-align: center;
        }
        .footer {
            margin-top: 30px;
        }
        .pad-10 {
            padding: 8px;
        }
        .text-right {
            text-align: right;
        }
        .page-break {
            page-break-after: always;
        }
        ul{
            line-height: 30px;font-size: 25px;
        }
        .list ul li {
            list-style: none;
        }

        .text-normal {
            font-weight: normal !important;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="field-section1">
        <div id="logo" class="logo-image">
            <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAiAAAAAZCAYAAAAIY5VQAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABeYSURBVHgB7V0HeBRV1z4zswtREBM6HwkEsfupCRAMFtgoBLABlscuiQ0rJBYsqAnWX32UgOW3EyxgBaJ+oqAm2AgkQLB3FwKCtERBjcnu3v++d7nLzOzMFgif6H/f51l2d26d2Yecd855zxmNFBQUFBQUWgsZucz0zU/11X1oVyEjt5L/64t8Z7SQVldv/56e6yeNeru2/wOxYcEAn05apflYKMDyuoysraK/AE77CbJgYdf8ZeUexkEKCiZoHKSgoKCgoLALoZPCPwaMhYhaNsfs88svv9Cff/5JCgoKCgoKfyUUAfmnoHkDsWWnEKvi3saVDxELbInq8tprr9GAAQMoJyeH3nvvPVJQUFBQUPir4DF/QTTmp59+ojVr1tCvv/5KPXv2pPT0dNprr72iBjY3N4v+bdq0oVgee3m37dZPtuu6Tl6v13GOUChELS0t5AbDMMjj8bjOnWj/eONijQ0EAuLd3vb777+La7pu3TpxDr169RLXNSUlJWp8MBiktm3bui0trjeuOyD7scBvPMo6hej7O0hn2/b95VWk1f8vsf3uJK3baPrhhx/o8ssvp7fffjsy17Bhw+jEE0+kqVOnUmZmJikoKCjsUvTMySLNSKUA+WldtT9m38zcTGohH2mUyf/wNXLjUUerq6toV0OuG0GwjtbU1Nm7rZ3H+5nQ1NTU2GdMXeOPc7JS26d4snBsa1OgDsdijesxMnwd4o0D1v8nJ0v3BkUfFtT9TpqOtfOyMrlXIct+nFv6rA3zBhALaY1dT3A+H6/R7JNzu+3BDuy7XRtvJvaFcQEK+HuMrPPH24+h6b35fnwaNCAwuHPmzKYpU8ros88+E0ZTonPnzjT4mGPo5ltupuzsfpHj2dnZ9PPPP9MTTzxBJ5xwguPmYDAHDRpEq1atomeffZaOO+44S/uGDRvoqKOOoq1btwrDvXz5curUqVPUPFVVVXT22WeTG0Bs/vWvf1F+fj5deeWV1KNHD7E2DCzmdOrfpUsXGjhwIJ1xxhlR+xo6dCh98cUXrmt17dpVGPCrrrqKMjIyxPGbbrqJNm/eTI8++qj47vf76f7776eKigpBLkDAgIaGBtp7773F+BtuuIH2228/cbyyspLOOecc+uijj6hPn2jN1m+//Ua33XYbPf/881RUVEQTxo8nL5+TrXmGtE/HkitGMHE+7777rmNzYWEhPf300/ZzVBoQBQWFHYNdhKqxqcSohP9lSY0cZVTFiUhhFBHpzg20h6Zz4uFzmNlPIZpMa6rLTWu1jgg1yXU3LcixaCdDQVasGTRKI806nlF5c7BlMoxyAzfyIU/wR0sz49dAY2OjxhEr7TSsdnJ4rQH82lGR5fqF+4AglMl+wMb5OeX8r7e7QeD76ZRfUyi/ChLgoZLo9a17tzeBWHg9nulO4xj3w4eatWIQnXj7EbfrMIRlZWXiAAgHXPTwSMDz8PXXX9PsOXNoLjekr7zyCo0ZM0b0W79+Pa1du5a2bNlCsYA7f/QDybDjhRdeoG+//ZZ7PjzcOxCg6ydOpCefeiqqH2eXYg6QlH333TeqvbGxkWpra8ULZOXDDz8Ux7E3jIMHJy0tTRyDlwHECV6JFStWCAJVXFxMpaWl1KFDB9EH7bm5uXTjjTda1gGp2bRpE3388cecsM2hN998k2pqamiPPfYQ2goQKgCejlGjRon36dOni7CHnBvkDuGPO+64Q5C4l156iY4//ngxN/bqpAnGmieddBJ98803dN999wnSIKExd8+QhPTOJNumoKCgsJPgHgxtStRRGHovVVKqL5saq8J32iABXrY82tCa5tI5SUjP7c1JxGRqLcAzo7PKnVlXNxzOEdCooI3h9TVUZmVTk0OzxuclzeG4XtBQmTsjFAyWcIteQM6TY7+lmxfkjNY8LXlpefE9FmZsendgCYVYqWuHbXvnZCPPTEI2vjNwrMZCZW7XC6TEaEPLN77dfwzFgf4iJwGSfNw8aZIIv8CIw0jiHZ6Ac889V5CRiy++WLS3Bv744w+65557xOeZM2cJD8GL3Bh/9dVXrmO6d+9OX375ZdQLhnvevHmCoIAcfPLJJ5Zxd999N61cuVK8Vq9eLUIs8PRIgjFlyhRBCMyAJwZEzPyCx2TkyJF0++23Cz0FyBOukR0gJdjDc889J7wr8HjAqYBXu3btBJlYvHgxXX311YLoxQLOCx4RkBkQLDP5UFBQUPgbI5PaNxVFvoGQuJOA7dColHoMGk2tBd2Yk/C66bk+ShY8jBT401OQcHdN82uGnsdt7lh38rEd/JY1K9DsKaIkAA9GTPIR2QxlchJSaR4Xi3yYNjWj8/ClcykO9Dvvukt8QIjjdm6EZahAAp6D8vJyHm64kk477TQRZmgNPMDDEyAzRx55JJ1yyil0wQUXCC/JUw4ekESQl5cnwipAvD3C6B9yyCF0Fz93kAkA3hiEORIFvCq4VlKTYQa8LInsAyGVESNGOLbBM/HQQw9SwdixgnS89dZbSquhoKDw94TGnO/OpXu+Z24BgZBYwCp46AN3XDOixhlsArUGnNYN73Uh35s/qr/G4hh6ttLpqMHDM7FG8XvTxvB7mHyk5fHQVMhpLT4/oxX2o7quTeBellSDh2R4n2KHccUhYnm61xAeHK/HO92+Pne+T0U/7o+psm6OMtfP71cgxuneUmfyYTpvTj54mEf0d9sP1hL7gQcBOO+888gNEF5Om/ag0DcceuihtLOAB+Kxxx8XnydPniwIwUUXXSS+P/jgg8JLkQwQtkBIRIZAevfunfBY6Cngmaivryd5LeIBISFcC7w7XQ94S9J79qSzzjqL7rzzTiH+/Pzzz0WYJpGyKxAAFxQUcNI3nvbp25ceeOABR22MQEcff+VFH+eBvVDva8VHkDsIX+2AVgakUkFBQWGXQeMGaNXiNG6N+jgQkUzxrx6lEyin+sWjhe6ivrqAG0Fr6AMhHIROdha6NtrhWDZf00d6SrYDCRniOpXH6NNpWG1mkFG2vY0xLdNpDAgHxnUcWpMGPYgkHyjexZuthl6jcszPjXsW00AWLEgNNhu+tPyauhBRlMg0FKC6LsNqqzC38GKQVevCAxzFnfNrivj8ZR2H1eTZSYhH08Xvo+l0eNTcnEhgXzgP3mOyJB+A634oKPbjwZ02CMCBBx5o6fD+++8LLYQdhx9+OO2///60M4DoEQb/4IMPFgJJoH///sIbgDv9Rx55RHgn7ICexC7QhEGHYYcOBDj11FNpn332oUTrq7Vv3154TqBpwZ6g1wDmzp1LdXVR101oOJBVgiyUxzmJwlp2IMOlmodYcB4gKggRSa8I2nD9cO5XXHEFHX300Zax8AqddOKJ9BtfZ9q0aTRhwgS68MILhRfKEXv2JRr4HrENbxF9No60Zk7eUgfzH2oG+X8KUPGoUXT66acLcoVrinAT9j5+/HgqKbmFjI0VxGpHkHbwND7Xzv2uCgoKClZwwrFqcTjG7+d39Rm5FfyTkyjRZx3GDb853BHk36Oi1cbOExBie1u+aqyO/Iv94rO/qpHSB1XwYyZvC7/7T/VxYmD1lrMQqxBeC46u3OhuXpBTxcznpNm9O2EEA6GpXYYuEeM4ASjfvo1QJmmGpW+ohUU8QUaLURbyBC1eIDeSY4fXy6+bzTyyIPNDkLp9LoiHTe2kZcHDwomM5ZrjvLsMD2fjbDv/UkoCQoQKfYc9/ABNxIIFC6IGwICZCUgsQ482ezuIgtR+wOuycePGSBuySt555x2RGgpRqD0chLkk0QCgI5EpswjlIBvnkksuoWQAYiDPfc8994wcR3bKySefbOk7Y8YMkdEDMoDsHaQouwEeB3g/4OGBZwYEB+QC3h0Iez/84AMaPHgw3cbbb77llsg4iFdBSiCO7datG7Vv144uGTdOEK+SkhLX9bQuI4gdvYLYljpa19SXHrnjMXr/g/f5uf0uvEtPPvmk+E3Dep4gHdrrD6Jl+UQNHyGXl9iiI/mmx5LWdxIpKCgotA60+MJIpL4G7cNshswpL8/FqCcFxgmFZrJRIe0XW4fo/ac2RYUguCejwfw9xGhlIrmETA8lLBxlRpxrqVF8HQuFiYp9a7rHWirdAanUlJJKHusPxRy8G8nAA4EkPAgfcIMIXYQEUkKRCSIBQeWPP27PIIKYEpBhDyfAW4C5AawDvPrqq0IMCoBkwEibAY8MXtJ4m4GQwaeffhr5jiyW0aNHC1Li8/no+uuvF+GiZLB06VJBQCBgPeywwyLHcS2QHWTGMcccI0SoIBDwKiQCzIt94wXvkRmXXXYZ/Q8nYxddfHHkGMI2yHSRZKiQh09Wcc8MrgdIUcx0ZG8qhbj340q+N2QCgfRgHqT8Ys9DhgwR4tkT+nPyUc3nMWXQaC2bOON/gOjn2aSgoKCw2yNcH4R2DiyN/iYIBo0IAUkbWe3ftCCH/u7QZQgAYkxkdUiMHTtWiCTxgpG36zJgjAGEU9wKd73xxhuCgMDl3y87W2RyICMFgEFG6CM1NdXyQj0PAOELJy2IuS8M6r333iuOw6uCNOFkgIyb888Pa1+OPfZYsadYgNcDIlkQo1hrgajBgxEPIDMgP+a6K9dcc43FEwPcwj0kKCR2/vnn8/WfjDknvESzZ8+mjh07ivRekDn8DiCKCHGJAmbBrRbyYUFLAykoKCj895ASfWcPrQj3IsR86cxPrQ3dFpLZjWAYwYiHo8FW0CwZ8PCOgycFItJ4r9aH57HHHhN3/qiLccQRR4iaGAgNQBuBLA6ERGB07ZVIcfeOoljwQsD7gJTWvn37CgMH7QjSR6FhAM4880zqwD0g06ZNpe+//15UA0WBMFkbw3IZuAFFOAUpp8hMufbaa2OeAMSaSIWdOXMmTZw4kQ466CCLJwNAcS/U6gBgkBEKWbZsmfASwHuCcIlMRY4HeD7gDcK+oFvBOdsBHQv0HQjXgFCALNmBawTBLbQ3WB+Fy9wAjc59990rfqPrrptI//73oeK3cgIEtUgXxvWD1wNeF1znRYsWibolIJYKCgoKuw2gteh1RKMIh0iEtKm0uro07tiM3J3LhtEIbn1zobJMa7uWRXbBBLQsZPU+cLuVZps38UwIJ+hao31ZHbqQbSGPgCeQqdvjUowSC+c4zN0c8PhkVdZYsHtdDN1dlJsIPDB+r7/+utAJQKh46623RnWSRm3JkiWRYxBrwjuCMEp1dbXQLjjhKE4mEFKA1gKZNADIC+7Q3XDppZeK/aCIl8yOcQPKt0MMikqiMPioVQLCYQ7FzJo1S7yckJWVRS+//LJjgTM3gLTB+4LsElQYtZdfx/khpRjemYcfflhoXUAEIEDFcdQJQdbOnpwUvc69E7HKr28/zzbiHFBXBJ4NhMwgZLUDBO67774T3g5k3kBIi3Xh4YEXBLoZBQUFhd0KTINh9UW+Q/jZM2dupAw6hJ97/VnCO6ZSfbV7MaR4Hgx7O9NW8LV82w9wEpSeW0IemkEBkBFmNWwQqTpA07UhyC5BwS5RXZSiKqomVSTMaPHWhWx6C34jWrL+Pzn+oG40GhSYEKW+ZO56DE1nEXLHPSlVoYBV0dvWE5zO918oC47hXNoY3jm6rpWlDV1iToPGGlnbl9R8G94aUNRlRG2ZrKraEggUOlVPddwX26YShWGE1wKGEcW84CnAnTPCBCABGRnp3JNxlhB5msWZMPovvviiGIcsEkwHbwNCNChzPnz4cNEPd/gyLDFp0qSoMIMZCOmA3ADwOEBvAuEn9gOdhxPgiUFVUQCppSAWEF46eRbgUUAJddQOkaXQzYDnBu2y6qsToB1BqAOaDOhFoK1A5VVoZ8zXBl4HXBuEe0DCQJhQARUkAnoPqaVBZg08TfCYxCJnEO2CAKFi7bhx46LaISjGfrAe0pEhGAbhgacHdUSw5+zeW4hqjhPiUztYhwGkH1WrSrErKCjsGOyl2Our+5jaysmeBVNfrfHjBfzTdIfZ/OE3GNCIh6Q8QkIyBs2NIglyTCBUTB6jIGa7oTdyR0I8AaYZYm17KfYIkLbLosWx3C4uNIKeAnsp9iALFnbNX1buNNXmBTnLmdNzXZyW1TR/x6FLxHVePz8ny9BoudPeWChU0Tl/aRGfu5JFkyRRRp3PlWk+B13TCiQJ2bBgYKlOrCTmZhj5m4MtkeqpTiXo0YdpzB8hIJY2fggEBHf2yTwWBMYPRjbZcf8fIK8NPDPxqp/uLECEQL6QdoswGkJqqE0C8hjx9PzxPbFvJ5O29nnkUhFLySCt91X8P/SlpHk7qB9PQUFhx7AjBAToNWg5Nz4JptayMqpfXByDuJAoYmZw4sJcyqRTaAzVL5kb9TwZN2giFTgPIRhXAuK2W0aFRtCoSoaAoBYID7MkRI4wvzmNd/M7A3/kdjwzumP4WTDJzA2g1gfqdiAVlwXbLHec27qOhYS47cfREoI84E49WRIhn2iryEc05LXZ1eQDQJVWaHngBYH3CsJfiHQtYaY9+pJ22DPEciqJ9S4iGlRD1Oc6HpTbixQUFBT+69DZGMfqo3bgybiGFi7EVV9dLr67zrmHezvTw2ERI2VMzDnEmiAfwTFh/UdygHfC8BpVlCRg8FHYK27HEJtsJh9AkIUK483NtNAY7I3igdHU8F5Q66OuEcXS4o3TdErljojMePvZ9dZwB4HMEPkCzCJYeGfsjht4F8w1QjBOZudgrJwLnghzm4Tsg3b5HZ8xp/ws92Dei+wjH+pmr1ViftibnNu+J7ywf9kX4817N4+X6yUCVE9FuAbp1G6kUOs4mLSDppDWthspKCgo/GWAcV8lvCUwVgsdeiwUFVFXLcq2EAG9bR73ckx1JC8QuKId5dzdysGjD+YMr2stc44xWBNVUf01riSF/9lfyF8znI5HSqvvADoNW1KKCqOYW5ZrF9sKl05fKKqQDq8ttY8DYUCbU9l2ic5Dl87F3gTJsfUT84dohpg/v6bI3IZzQbhHPMnXaRwnLJphZEvSEms/jiGY3QHQTkBACS0HwgfQUUCXAU3K/PnzRSgDgkwAmTXIqunatSt99923NGSIT/SBBgLGGlkoMvyBVFtUeT3ggANEGzJ0gGeeeUboKmD4kUmDPjDgmBOpxCi5DnEntCsgAUjJBWbNmknp6RliLmTFLPr4Y+rWvbt4GB00JiirLjNWIEhFdgyOQdALbwgyeEAQsBaeK4MMHWQJQROCbCBk90AvgsweZNdASIrMmYaGBnF8V0BTLiwFBYUdRaYtRdRMFjJRRdRWyCuWV0H2T9TzYFk7pVGQi2TazWu6tZPIBrHazW2hjYZKX2ogsDWLMa3R623nT8uzjl9rS59NSUlptPeJBcyP92THNDWFr3m8TBfsL9k9JTvOvB8P7aaQNSzwjuwckA8U1gJfAkkw8yYUJ0MJdthN6B/QDlKC0u0owgWjDk8CCAYergcygawQiG5h9FFxFWmxyBDBOwgPSAdCFpgboROIVSsqKqgdJyAlpaWRtbt16y76YA6Qiq7duolKplgLJALpzBKyKBuA+h/IHIJGA+cIsSzEoijYBuIBUgJha79+/cSeULwNQmGQKQhuZWE3BQUFhd0KschC2KAnbtyS7h+HqCRCZJJd04RtBrjKrT2RVNcE5t+RMQmN29H9JTPOvJ/dloAAMpUWRAHPjIExl2mv8ACgjgdIBzwC8GAcxN+ReQLvBrJRUOgMXhQYbaS6YtymTZsEoQGBgWGX5d6R0opS7sjUAeHAcazRv38/Wrt2nSA/IArwukDgiXRfzAcCIUkCSrPL9RYuXCgygOBJgacDxAfPjUH6LIgECBWAteS5IvsFe6uuXsQJyUqRugvyhDVR8A1ZQPgOUSnmxTkoZ4WCgoKCwt8Ru20IBpDeCXg0ZK0MHMOWEVKR+hCERWCIEZ5AyAR9ZTYOxpr7Iu0V3gf0h8GXBhxhFRAFmQGEJ92C8KAaK+bDvJgDa6ENxzAW/c19sB4ePofP6CufdYPzAIGAVwbroB2QcwLyPDEeqcwgMnJf2BPOHUQF7bIa7K6ACsEoKCgouMMtBEMKSWG3JiAKfw0UAVFQUFBwhyIgrYP/A7oYLVdFUU+pAAAAAElFTkSuQmCC"
            />
        </div>
    </div>
    <div class="field-section">
        <p>
            <strong>ACCIDENTAL DEATH POLICY BENEFIT SCHEDULE </strong><br />
            <strong>THIS IS AN IMPORTANT DOCUMENT ABOUT THE INSURANCE COVER TAKEN UP AND YOU SHOULD STUDY IT CAREFULLY AND ENSURE THAT YOU UNDERSTAND THE CONTENT.</strong><br />
            You have applied for an Accidental Death Policy with Hello Protect Pty Ltd (FSP49260) which policy is underwritten by Guardrisk Microinsurance Limited, an authorized financial services provider (FSP No 51674) and a licensed insurer.
        </p>
    </div>
    <div class="field-section">
        <table>
            <tbody>
            <tr>
                <td><strong>Policy Number</strong></td>
                <td id="PolicyNumber">@isset($replaceParamsArr['policyNumber']) {{ $replaceParamsArr['policyNumber'] }}@endisset</td>
                <td><strong>Inception Date</strong></td>
                <td id="CoverStartDate">@isset($replaceParamsArr['inceptionDate']){{ $replaceParamsArr['inceptionDate'] }}@endisset</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td><strong>End Date</strong></td>
                <td id="coverEndDate">@isset($replaceParamsArr['endDate']) {{$replaceParamsArr['endDate'] }}@endisset </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="field-section">
        <table>
            <tbody>
            <tr>
                <td><strong>Insured Name and Surname</strong></td>
                <td id="insured" colspan="3"> @isset($replaceParamsArr['customerFirstName']) {{$replaceParamsArr['customerFirstName'] }}@endisset
                    @isset($replaceParamsArr['customerSurname'])  {{$replaceParamsArr['customerSurname'] }}@endisset </td>
            </tr>
            <tr>
                <td><strong>Email Address</strong></td>
                <td id="emailAddress" colspan="3">@isset($replaceParamsArr['customerEmail'])  {{$replaceParamsArr['customerEmail'] }} @endisset</td>
            </tr>
            <tr>
                <td><strong>Date of Birth</strong></td>
                <td id="dateOfBirth">@isset($replaceParamsArr['customerDOB'])  {{$replaceParamsArr['customerDOB'] }}@endisset</td>
                <td><strong>Cell Number</strong></td>
                <td id="cellNumber"> @isset($replaceParamsArr['customerMsisdn'])  {{$replaceParamsArr['customerMsisdn'] }}@endisset </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="field-section">
        <table>
            <tbody>
            <tr>
                <td><strong>Beneficiary Name and Surname</strong></td>
                <td id="beneficiaryName" colspan="3"> @isset($replaceParamsArr['beneficiaryFirstName'])  {{$replaceParamsArr['beneficiaryFirstName'] }}@endisset
                    @isset($replaceParamsArr['beneficiaryLastName'])  {{$replaceParamsArr['beneficiaryLastName'] }}@endisset
                </td>
            </tr>
            <tr>
                <td><strong>Contact Number</strong></td>
                <td id="dateOfBirth"> @isset($replaceParamsArr['beneficiaryMsisdn'])  {{$replaceParamsArr['beneficiaryMsisdn'] }}@endisset </td>
                <td><strong>Relationship</strong></td>
                <td id="contactNumber"> @isset($replaceParamsArr['beneficiaryRelationWithCustomer'])  {{$replaceParamsArr['beneficiaryRelationWithCustomer'] }}@endisset </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="field-section">
        <table>
            <tr>
                <td><strong>Item</strong></td>
                <td><strong>Total</strong></td>
            </tr>
            <tr>
                <td><strong>Policy Premium</strong></td>
                <td id="policyPremiumTotal">@isset($replaceParamsArr['policyPremiumTotal']) R {{$replaceParamsArr['policyPremiumTotal'] }} @endisset</td>
            </tr>
            <tr>
                <td><strong>Total</strong></td>
                <td id="totalTotal">@isset($replaceParamsArr['policyTotalPremiumTotal']) R  {{$replaceParamsArr['policyTotalPremiumTotal'] }}@endisset</td>
            </tr>
        </table>
    </div>
    <div class="field-section">
        <table>
            <tr>
                <td><strong>Cover Amount</strong></td>
                <td class="text-right" colspan="4">@isset($replaceParamsArr['policyCoverAmount']) R {{ $replaceParamsArr['policyCoverAmount']  }} @endisset</td>
            </tr>
        </table>
    </div>
    <div class="field-section2">
        <h2>Policy Information:</h2>
        <table>
            <tr>
                <td><strong>Type of Cover:</strong></td>
                <td>Accidental Death Policy</td>
            </tr>
            <tr>
                <td><strong>Risk Class:</strong></td>
                <td>Risk Class Individual Death</td>
            </tr>
            <tr>
                <td><strong>Claimable event:</strong></td>
                <td>The Cover amount will be paid upon the death of the Policyholder due to accidental causes</td>
            </tr>
            <tr>
                <td><strong>Cession:</strong></td>
                <td>No cession is available for this policy</td>
            </tr>
        </table>
    </div>
    <div class="field-section2">
        <div class="field-section3">
            <p>1. INTRODUCTION</p>
            <ul  style="list-style-type: none;">
                <li>
                    1.1 You have voluntarily applied for this Accidental Death Policy.
                    This Policy is optional and has been taken out because it meets
                    your financial needs.
                </li>
            </ul>
        </div>
        <div class="field-section3">
            <p>2. DETAILS AND DECRIPTION OF THE TRANSACTION</p>
            <ul style="list-style-type: none;">
                <li>
                    2.1 Your Intermediary, an authorised Financial Services Provider, is
                    Hello Paisa Pty Ltd with FSP number 48992. Your
                    Administrator Hello Protect Pty Ltd, an authorised Financial Services
                    Provider, with FSP number 49260.
                </li>
                <li>
                    2.2 Hello Paisa Pty Ltd provides an intermediary service
                    in terms of the Financial Advisory and Intermediary Services Act
                    37 of 2002 (hereinafter refer to as ‘’FAIS’’).
                </li>
                <li>2.3 This schedule contains proof of cover.</li>
                <li>
                    2.4 You can log into your personal profile at any time to manage
                    this Policy. Alternatively, you can contact the call centre on
                    012 880 1800 should You require further
                    information or advice relating to this product.
                </li>
                <li>
                    2.5 Guardrisk Microinsurance Limited is contactable on Tel: 011
                    669 1000 ; Email: info@guardrisk.co.za / website:
                    www.guardrisk.co.za / Complaints: complaints@guardrisk.co.za
                </li>
                <li>
                    2.6 Premiums are guaranteed for the first Period of Insurance from the Entry Date. For any subsequent Period of Insurance where the Policy Auto-Renews. The Policy may be reviewed by the Insurer and may be amended or changed
                </li>
                <li>
                    2.7 The Premium shall cover the Benefit Amount selected for a 31 (thirty-one) Day period.
                </li>
                <li>
                    2.8 Should you need to complain, you must contact the
                    Administrator first, If you remain dissatisfied with the
                    Administrator’s response or handling of your complaint, you can
                    escalate a complaint to Guardrisk Microinsurance Limited (details
                    provided under 2.6). Should you still remain dissatisfied with the
                    Insurer’s response, you can then escalate a complaint to the
                    office of the Long Term Insurance Ombudsman (for claims /service
                    related matters) on Email: info@ombud.co.za / Tel: 021 657 5000 or
                    the office of the FAIS Ombud for (advice related matters) on
                    Email: info@faisombud.co.za / Tel: 012 762 5000 or the Information
                    Regulator (for any personal information breach) on Email:
                    complaints.IR@justice.gov.za / Tel: 010 023 5200
                </li>
            </ul>
        </div>
    </div>


    <div id="footer" class="footer">
        <div style="width: 100%; display: table">
            <table  style="border: none;">
                <tbody>
                <tr>
                    <td style="text-align: left; border: none;">Underwritten by @isset($replaceParamsArr['underwriterName'])  {{$replaceParamsArr['underwriterName']  }} @endisset </td>
                    <td style="text-align: right; border: none;">Printed on: @isset($replaceParamsArr['year']){{$replaceParamsArr['year']}}@endisset-@isset($replaceParamsArr['month']){{$replaceParamsArr['month']}}@endisset-@isset($replaceParamsArr['day']){{$replaceParamsArr['day']}}@endisset
                    </td>
                </tr>
                </tbody>
            </table>
            </table>
        </div>
    </div>
</div>

<div class="page-break"></div>

<div class="container">
    <div class="field-section1">
        <div id="logo" class="logo-image">
            <img
                src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAiAAAAAZCAYAAAAIY5VQAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAABeYSURBVHgB7V0HeBRV1z4zswtREBM6HwkEsfupCRAMFtgoBLABlscuiQ0rJBYsqAnWX32UgOW3EyxgBaJ+oqAm2AgkQLB3FwKCtERBjcnu3v++d7nLzOzMFgif6H/f51l2d26d2Yecd855zxmNFBQUFBQUWgsZucz0zU/11X1oVyEjt5L/64t8Z7SQVldv/56e6yeNeru2/wOxYcEAn05apflYKMDyuoysraK/AE77CbJgYdf8ZeUexkEKCiZoHKSgoKCgoLALoZPCPwaMhYhaNsfs88svv9Cff/5JCgoKCgoKfyUUAfmnoHkDsWWnEKvi3saVDxELbInq8tprr9GAAQMoJyeH3nvvPVJQUFBQUPir4DF/QTTmp59+ojVr1tCvv/5KPXv2pPT0dNprr72iBjY3N4v+bdq0oVgee3m37dZPtuu6Tl6v13GOUChELS0t5AbDMMjj8bjOnWj/eONijQ0EAuLd3vb777+La7pu3TpxDr169RLXNSUlJWp8MBiktm3bui0trjeuOyD7scBvPMo6hej7O0hn2/b95VWk1f8vsf3uJK3baPrhhx/o8ssvp7fffjsy17Bhw+jEE0+kqVOnUmZmJikoKCjsUvTMySLNSKUA+WldtT9m38zcTGohH2mUyf/wNXLjUUerq6toV0OuG0GwjtbU1Nm7rZ3H+5nQ1NTU2GdMXeOPc7JS26d4snBsa1OgDsdijesxMnwd4o0D1v8nJ0v3BkUfFtT9TpqOtfOyMrlXIct+nFv6rA3zBhALaY1dT3A+H6/R7JNzu+3BDuy7XRtvJvaFcQEK+HuMrPPH24+h6b35fnwaNCAwuHPmzKYpU8ros88+E0ZTonPnzjT4mGPo5ltupuzsfpHj2dnZ9PPPP9MTTzxBJ5xwguPmYDAHDRpEq1atomeffZaOO+44S/uGDRvoqKOOoq1btwrDvXz5curUqVPUPFVVVXT22WeTG0Bs/vWvf1F+fj5deeWV1KNHD7E2DCzmdOrfpUsXGjhwIJ1xxhlR+xo6dCh98cUXrmt17dpVGPCrrrqKMjIyxPGbbrqJNm/eTI8++qj47vf76f7776eKigpBLkDAgIaGBtp7773F+BtuuIH2228/cbyyspLOOecc+uijj6hPn2jN1m+//Ua33XYbPf/881RUVEQTxo8nL5+TrXmGtE/HkitGMHE+7777rmNzYWEhPf300/ZzVBoQBQWFHYNdhKqxqcSohP9lSY0cZVTFiUhhFBHpzg20h6Zz4uFzmNlPIZpMa6rLTWu1jgg1yXU3LcixaCdDQVasGTRKI806nlF5c7BlMoxyAzfyIU/wR0sz49dAY2OjxhEr7TSsdnJ4rQH82lGR5fqF+4AglMl+wMb5OeX8r7e7QeD76ZRfUyi/ChLgoZLo9a17tzeBWHg9nulO4xj3w4eatWIQnXj7EbfrMIRlZWXiAAgHXPTwSMDz8PXXX9PsOXNoLjekr7zyCo0ZM0b0W79+Pa1du5a2bNlCsYA7f/QDybDjhRdeoG+//ZZ7PjzcOxCg6ydOpCefeiqqH2eXYg6QlH333TeqvbGxkWpra8ULZOXDDz8Ux7E3jIMHJy0tTRyDlwHECV6JFStWCAJVXFxMpaWl1KFDB9EH7bm5uXTjjTda1gGp2bRpE3388cecsM2hN998k2pqamiPPfYQ2goQKgCejlGjRon36dOni7CHnBvkDuGPO+64Q5C4l156iY4//ngxN/bqpAnGmieddBJ98803dN999wnSIKExd8+QhPTOJNumoKCgsJPgHgxtStRRGHovVVKqL5saq8J32iABXrY82tCa5tI5SUjP7c1JxGRqLcAzo7PKnVlXNxzOEdCooI3h9TVUZmVTk0OzxuclzeG4XtBQmTsjFAyWcIteQM6TY7+lmxfkjNY8LXlpefE9FmZsendgCYVYqWuHbXvnZCPPTEI2vjNwrMZCZW7XC6TEaEPLN77dfwzFgf4iJwGSfNw8aZIIv8CIw0jiHZ6Ac889V5CRiy++WLS3Bv744w+65557xOeZM2cJD8GL3Bh/9dVXrmO6d+9OX375ZdQLhnvevHmCoIAcfPLJJ5Zxd999N61cuVK8Vq9eLUIs8PRIgjFlyhRBCMyAJwZEzPyCx2TkyJF0++23Cz0FyBOukR0gJdjDc889J7wr8HjAqYBXu3btBJlYvHgxXX311YLoxQLOCx4RkBkQLDP5UFBQUPgbI5PaNxVFvoGQuJOA7dColHoMGk2tBd2Yk/C66bk+ShY8jBT401OQcHdN82uGnsdt7lh38rEd/JY1K9DsKaIkAA9GTPIR2QxlchJSaR4Xi3yYNjWj8/ClcykO9Dvvukt8QIjjdm6EZahAAp6D8vJyHm64kk477TQRZmgNPMDDEyAzRx55JJ1yyil0wQUXCC/JUw4ekESQl5cnwipAvD3C6B9yyCF0Fz93kAkA3hiEORIFvCq4VlKTYQa8LInsAyGVESNGOLbBM/HQQw9SwdixgnS89dZbSquhoKDw94TGnO/OpXu+Z24BgZBYwCp46AN3XDOixhlsArUGnNYN73Uh35s/qr/G4hh6ttLpqMHDM7FG8XvTxvB7mHyk5fHQVMhpLT4/oxX2o7quTeBellSDh2R4n2KHccUhYnm61xAeHK/HO92+Pne+T0U/7o+psm6OMtfP71cgxuneUmfyYTpvTj54mEf0d9sP1hL7gQcBOO+888gNEF5Om/ag0DcceuihtLOAB+Kxxx8XnydPniwIwUUXXSS+P/jgg8JLkQwQtkBIRIZAevfunfBY6Cngmaivryd5LeIBISFcC7w7XQ94S9J79qSzzjqL7rzzTiH+/Pzzz0WYJpGyKxAAFxQUcNI3nvbp25ceeOABR22MQEcff+VFH+eBvVDva8VHkDsIX+2AVgakUkFBQWGXQeMGaNXiNG6N+jgQkUzxrx6lEyin+sWjhe6ivrqAG0Fr6AMhHIROdha6NtrhWDZf00d6SrYDCRniOpXH6NNpWG1mkFG2vY0xLdNpDAgHxnUcWpMGPYgkHyjexZuthl6jcszPjXsW00AWLEgNNhu+tPyauhBRlMg0FKC6LsNqqzC38GKQVevCAxzFnfNrivj8ZR2H1eTZSYhH08Xvo+l0eNTcnEhgXzgP3mOyJB+A634oKPbjwZ02CMCBBx5o6fD+++8LLYQdhx9+OO2///60M4DoEQb/4IMPFgJJoH///sIbgDv9Rx55RHgn7ICexC7QhEGHYYcOBDj11FNpn332oUTrq7Vv3154TqBpwZ6g1wDmzp1LdXVR101oOJBVgiyUxzmJwlp2IMOlmodYcB4gKggRSa8I2nD9cO5XXHEFHX300Zax8AqddOKJ9BtfZ9q0aTRhwgS68MILhRfKEXv2JRr4HrENbxF9No60Zk7eUgfzH2oG+X8KUPGoUXT66acLcoVrinAT9j5+/HgqKbmFjI0VxGpHkHbwND7Xzv2uCgoKClZwwrFqcTjG7+d39Rm5FfyTkyjRZx3GDb853BHk36Oi1cbOExBie1u+aqyO/Iv94rO/qpHSB1XwYyZvC7/7T/VxYmD1lrMQqxBeC46u3OhuXpBTxcznpNm9O2EEA6GpXYYuEeM4ASjfvo1QJmmGpW+ohUU8QUaLURbyBC1eIDeSY4fXy6+bzTyyIPNDkLp9LoiHTe2kZcHDwomM5ZrjvLsMD2fjbDv/UkoCQoQKfYc9/ABNxIIFC6IGwICZCUgsQ482ezuIgtR+wOuycePGSBuySt555x2RGgpRqD0chLkk0QCgI5EpswjlIBvnkksuoWQAYiDPfc8994wcR3bKySefbOk7Y8YMkdEDMoDsHaQouwEeB3g/4OGBZwYEB+QC3h0Iez/84AMaPHgw3cbbb77llsg4iFdBSiCO7datG7Vv144uGTdOEK+SkhLX9bQuI4gdvYLYljpa19SXHrnjMXr/g/f5uf0uvEtPPvmk+E3Dep4gHdrrD6Jl+UQNHyGXl9iiI/mmx5LWdxIpKCgotA60+MJIpL4G7cNshswpL8/FqCcFxgmFZrJRIe0XW4fo/ac2RYUguCejwfw9xGhlIrmETA8lLBxlRpxrqVF8HQuFiYp9a7rHWirdAanUlJJKHusPxRy8G8nAA4EkPAgfcIMIXYQEUkKRCSIBQeWPP27PIIKYEpBhDyfAW4C5AawDvPrqq0IMCoBkwEibAY8MXtJ4m4GQwaeffhr5jiyW0aNHC1Li8/no+uuvF+GiZLB06VJBQCBgPeywwyLHcS2QHWTGMcccI0SoIBDwKiQCzIt94wXvkRmXXXYZ/Q8nYxddfHHkGMI2yHSRZKiQh09Wcc8MrgdIUcx0ZG8qhbj340q+N2QCgfRgHqT8Ys9DhgwR4tkT+nPyUc3nMWXQaC2bOON/gOjn2aSgoKCw2yNcH4R2DiyN/iYIBo0IAUkbWe3ftCCH/u7QZQgAYkxkdUiMHTtWiCTxgpG36zJgjAGEU9wKd73xxhuCgMDl3y87W2RyICMFgEFG6CM1NdXyQj0PAOELJy2IuS8M6r333iuOw6uCNOFkgIyb888Pa1+OPfZYsadYgNcDIlkQo1hrgajBgxEPIDMgP+a6K9dcc43FEwPcwj0kKCR2/vnn8/WfjDknvESzZ8+mjh07ivRekDn8DiCKCHGJAmbBrRbyYUFLAykoKCj895ASfWcPrQj3IsR86cxPrQ3dFpLZjWAYwYiHo8FW0CwZ8PCOgycFItJ4r9aH57HHHhN3/qiLccQRR4iaGAgNQBuBLA6ERGB07ZVIcfeOoljwQsD7gJTWvn37CgMH7QjSR6FhAM4880zqwD0g06ZNpe+//15UA0WBMFkbw3IZuAFFOAUpp8hMufbaa2OeAMSaSIWdOXMmTZw4kQ466CCLJwNAcS/U6gBgkBEKWbZsmfASwHuCcIlMRY4HeD7gDcK+oFvBOdsBHQv0HQjXgFCALNmBawTBLbQ3WB+Fy9wAjc59990rfqPrrptI//73oeK3cgIEtUgXxvWD1wNeF1znRYsWibolIJYKCgoKuw2gteh1RKMIh0iEtKm0uro07tiM3J3LhtEIbn1zobJMa7uWRXbBBLQsZPU+cLuVZps38UwIJ+hao31ZHbqQbSGPgCeQqdvjUowSC+c4zN0c8PhkVdZYsHtdDN1dlJsIPDB+r7/+utAJQKh46623RnWSRm3JkiWRYxBrwjuCMEp1dbXQLjjhKE4mEFKA1gKZNADIC+7Q3XDppZeK/aCIl8yOcQPKt0MMikqiMPioVQLCYQ7FzJo1S7yckJWVRS+//LJjgTM3gLTB+4LsElQYtZdfx/khpRjemYcfflhoXUAEIEDFcdQJQdbOnpwUvc69E7HKr28/zzbiHFBXBJ4NhMwgZLUDBO67774T3g5k3kBIi3Xh4YEXBLoZBQUFhd0KTINh9UW+Q/jZM2dupAw6hJ97/VnCO6ZSfbV7MaR4Hgx7O9NW8LV82w9wEpSeW0IemkEBkBFmNWwQqTpA07UhyC5BwS5RXZSiKqomVSTMaPHWhWx6C34jWrL+Pzn+oG40GhSYEKW+ZO56DE1nEXLHPSlVoYBV0dvWE5zO918oC47hXNoY3jm6rpWlDV1iToPGGlnbl9R8G94aUNRlRG2ZrKraEggUOlVPddwX26YShWGE1wKGEcW84CnAnTPCBCABGRnp3JNxlhB5msWZMPovvviiGIcsEkwHbwNCNChzPnz4cNEPd/gyLDFp0qSoMIMZCOmA3ADwOEBvAuEn9gOdhxPgiUFVUQCppSAWEF46eRbgUUAJddQOkaXQzYDnBu2y6qsToB1BqAOaDOhFoK1A5VVoZ8zXBl4HXBuEe0DCQJhQARUkAnoPqaVBZg08TfCYxCJnEO2CAKFi7bhx46LaISjGfrAe0pEhGAbhgacHdUSw5+zeW4hqjhPiUztYhwGkH1WrSrErKCjsGOyl2Our+5jaysmeBVNfrfHjBfzTdIfZ/OE3GNCIh6Q8QkIyBs2NIglyTCBUTB6jIGa7oTdyR0I8AaYZYm17KfYIkLbLosWx3C4uNIKeAnsp9iALFnbNX1buNNXmBTnLmdNzXZyW1TR/x6FLxHVePz8ny9BoudPeWChU0Tl/aRGfu5JFkyRRRp3PlWk+B13TCiQJ2bBgYKlOrCTmZhj5m4MtkeqpTiXo0YdpzB8hIJY2fggEBHf2yTwWBMYPRjbZcf8fIK8NPDPxqp/uLECEQL6QdoswGkJqqE0C8hjx9PzxPbFvJ5O29nnkUhFLySCt91X8P/SlpHk7qB9PQUFhx7AjBAToNWg5Nz4JptayMqpfXByDuJAoYmZw4sJcyqRTaAzVL5kb9TwZN2giFTgPIRhXAuK2W0aFRtCoSoaAoBYID7MkRI4wvzmNd/M7A3/kdjwzumP4WTDJzA2g1gfqdiAVlwXbLHec27qOhYS47cfREoI84E49WRIhn2iryEc05LXZ1eQDQJVWaHngBYH3CsJfiHQtYaY9+pJ22DPEciqJ9S4iGlRD1Oc6HpTbixQUFBT+69DZGMfqo3bgybiGFi7EVV9dLr67zrmHezvTw2ERI2VMzDnEmiAfwTFh/UdygHfC8BpVlCRg8FHYK27HEJtsJh9AkIUK483NtNAY7I3igdHU8F5Q66OuEcXS4o3TdErljojMePvZ9dZwB4HMEPkCzCJYeGfsjht4F8w1QjBOZudgrJwLnghzm4Tsg3b5HZ8xp/ws92Dei+wjH+pmr1ViftibnNu+J7ywf9kX4817N4+X6yUCVE9FuAbp1G6kUOs4mLSDppDWthspKCgo/GWAcV8lvCUwVgsdeiwUFVFXLcq2EAG9bR73ckx1JC8QuKId5dzdysGjD+YMr2stc44xWBNVUf01riSF/9lfyF8znI5HSqvvADoNW1KKCqOYW5ZrF9sKl05fKKqQDq8ttY8DYUCbU9l2ic5Dl87F3gTJsfUT84dohpg/v6bI3IZzQbhHPMnXaRwnLJphZEvSEms/jiGY3QHQTkBACS0HwgfQUUCXAU3K/PnzRSgDgkwAmTXIqunatSt99923NGSIT/SBBgLGGlkoMvyBVFtUeT3ggANEGzJ0gGeeeUboKmD4kUmDPjDgmBOpxCi5DnEntCsgAUjJBWbNmknp6RliLmTFLPr4Y+rWvbt4GB00JiirLjNWIEhFdgyOQdALbwgyeEAQsBaeK4MMHWQJQROCbCBk90AvgsweZNdASIrMmYaGBnF8V0BTLiwFBYUdRaYtRdRMFjJRRdRWyCuWV0H2T9TzYFk7pVGQi2TazWu6tZPIBrHazW2hjYZKX2ogsDWLMa3R623nT8uzjl9rS59NSUlptPeJBcyP92THNDWFr3m8TBfsL9k9JTvOvB8P7aaQNSzwjuwckA8U1gJfAkkw8yYUJ0MJdthN6B/QDlKC0u0owgWjDk8CCAYergcygawQiG5h9FFxFWmxyBDBOwgPSAdCFpgboROIVSsqKqgdJyAlpaWRtbt16y76YA6Qiq7duolKplgLJALpzBKyKBuA+h/IHIJGA+cIsSzEoijYBuIBUgJha79+/cSeULwNQmGQKQhuZWE3BQUFhd0KschC2KAnbtyS7h+HqCRCZJJd04RtBrjKrT2RVNcE5t+RMQmN29H9JTPOvJ/dloAAMpUWRAHPjIExl2mv8ACgjgdIBzwC8GAcxN+ReQLvBrJRUOgMXhQYbaS6YtymTZsEoQGBgWGX5d6R0opS7sjUAeHAcazRv38/Wrt2nSA/IArwukDgiXRfzAcCIUkCSrPL9RYuXCgygOBJgacDxAfPjUH6LIgECBWAteS5IvsFe6uuXsQJyUqRugvyhDVR8A1ZQPgOUSnmxTkoZ4WCgoKCwt8Ru20IBpDeCXg0ZK0MHMOWEVKR+hCERWCIEZ5AyAR9ZTYOxpr7Iu0V3gf0h8GXBhxhFRAFmQGEJ92C8KAaK+bDvJgDa6ENxzAW/c19sB4ePofP6CufdYPzAIGAVwbroB2QcwLyPDEeqcwgMnJf2BPOHUQF7bIa7K6ACsEoKCgouMMtBEMKSWG3JiAKfw0UAVFQUFBwhyIgrYP/A7oYLVdFUU+pAAAAAElFTkSuQmCC"
            />


        </div>
    </div>
    <div class="field-section">
        <p>
            <strong>ACCIDENTAL DEATH POLICY BENEFIT SCHEDULE </strong><br />
            <strong>THIS IS AN IMPORTANT DOCUMENT ABOUT THE INSURANCE COVER TAKEN UP AND YOU SHOULD STUDY IT CAREFULLY AND ENSURE THAT YOU UNDERSTAND THE CONTENT.</strong><br />
            You have applied for an Accidental Death Policy with Hello Protect Pty Ltd (FSP49260) which policy is underwritten by Guardrisk Microinsurance Limited, an authorized financial services provider (FSP No 51674) and a licensed insurer.
        </p>
    </div>
    <div class="field-section">
        <table>
            <thead>
            <tr>
                <td>Personal Accident</td>
                <td>The Cover amount will be paid upon the death of the Policyholder due to accidental causes</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Insured Name</td>
                <td id="insuredName">@isset($replaceParamsArr['customerFirstName'])   {{$replaceParamsArr['customerFirstName']  }} @endisset</td>
            </tr>
            <tr>
                <td>Insured Surname</td>
                <td id="insuredSurname">@isset($replaceParamsArr['customerSurname'])   {{$replaceParamsArr['customerSurname']  }} @endisset</td>
            </tr>
            <tr>
                <td>ID or Passport Number</td>
                <td id="idOrPassportNumber">@isset($replaceParamsArr['customerIdNumber'])   {{$replaceParamsArr['customerIdNumber']  }} @endisset</td>
            </tr>
            <tr>
                <td>Sum Insured</td>
                <td id="sumInsured">@isset($replaceParamsArr['policyCoverAmount']) R  {{$replaceParamsArr['policyCoverAmount']  }} @endisset</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="field-section2">
        <div class="field-section3 list">
            <p>3. WAITING PERIODS AND EXCLUSIONS AND FEES</p>
            <p class="text-normal">
                You acknowledge, understand and confirm that the following has been
                taken into consideration with your application:
            </p>
            <ul>
                <li>3.1.1 this policy meets your financial needs;</li>
                <li>
                    3.1.2 you understand the benefits applicable under this Policy;
                </li>
                <li>3.1.3 there is no waiting period for accidental death;</li>
                <li>
                    3.1.4 this Policy excludes the following:
                    <ul>
                        <li>
                            3.1.4.1 Intentionally self-inflicted injury, suicide or a
                            suicide attempt (whether sane or insane) within the <br />
                            first 12 (twelve) months from the Entry Date;
                        </li>
                        <li>
                            3.1.4.2 The Insured’s participation in any criminal
                            activities;
                        </li>
                        <li>
                            3.1.4.3 No payment will be made under this Policy if the
                            premiums have not been paid up to date or if the <br />Insured
                            has not complied with all the obligations and <br />conditions
                            of this Policy;
                        </li>
                        <li>
                            3.1.4.4 Nuclear accidents, radioactivity, war or armed
                            conflict (whether war be declared or not), terrorist or
                            insurgency activities, rebellion, civil commotion,
                            sedation,<br />
                            sabotage or any activity associated with the aforegoing or the
                            defense, quelling investigation or <br />
                            containment thereof by any security force;
                        </li>
                    </ul>
                </li>
                <li>
                    3.1.5 an intermediary fee of 3.25% and a binder fee of 9% is
                    included in your monthly premium.
                </li>
            </ul>
        </div>
    </div>
    <div class="field-section2">
        <div class="field-section3">
            <p>PROCESSING OF PERSONAL INFORMATION</p>
            <p class="text-normal">
                Processing of Personal Information in terms of the Protection of
                Personal Information Act 4 of 2013 Your privacy is of utmost
                importance to Us. We will take the necessary measures to ensure that
                any and all information, provided by you for the purpose of this
                application, is processed in accordance with the provisions of the
                Protection of Personal Information Act 4 of 2013 and further, is
                stored in a safe and secure manner. You hereby agree to give honest,
                accurate and up-to-date Personal Information in order to process and
                accept this application.
            </p>
            <p class="text-normal">
                You accept that your Personal Information collected by Us may be
                used for the following reasons:
            </p>
            <ul>
                <li>
                    to establish and verify your identity in terms of the Applicable
                    Laws;
                </li>
                <li>to enable Us to issue and administer this Policy;</li>
            </ul>
            <p class="text-normal">
                Unless consented to by yourself, we will not sell, exchange,
                transfer, rent or otherwise make available your Personal Information
                (such as your name, address, email address, telephone or fax number)
                to any other parties and you indemnify Us from any claims resulting
                from disclosures made with your consent.
            </p>
            <p class="text-normal">
                You understand that if the Administrator/Guardrisk Microinsurance
                Limited has utilised your Personal Information contrary to the
                Applicable Laws, you have the right to lodge a complaint with
                Guardrisk Microinsurance Limited or with the Information Regulator
            </p>
        </div>
    </div>
    <div class="field-section">
        <table>
            <tbody>
            <tr>
                <td class="pad-10">
                    Signed by Sayjil Magan<br />
                    Managing Director<br />
                    Hello Protect Pty Ltd              </td>
                <td class="pad-10"><i>Sayjil Magan</i></td>
                <td class="pad-10"><strong>Date</strong></td>
                <td class="pad-10" id="createDate"> @isset($replaceParamsArr['createdDate'])  {{$replaceParamsArr['createdDate']  }} @endisset </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="footer" class="footer">
        <div style="width: 100%; display: table">
            <table  style="border: none;">
                <tbody>
                <tr>
                    <td style="text-align: left; border: none;">Underwritten by @isset($replaceParamsArr['underwriterName'])  {{$replaceParamsArr['underwriterName']  }} @endisset </td>
                    <td style="text-align: right; border: none;">Printed on: @isset($replaceParamsArr['year']){{$replaceParamsArr['year']}}@endisset-@isset($replaceParamsArr['month']){{$replaceParamsArr['month']}}@endisset-@isset($replaceParamsArr['day']){{$replaceParamsArr['day']}}@endisset
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>
