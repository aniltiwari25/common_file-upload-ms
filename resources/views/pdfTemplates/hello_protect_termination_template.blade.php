<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Hello Protect Embedded Benefit </title>
  <style>

  </style>
</head>

<body style="padding: 0; margin: 0;">
  <div class="main-body" style="width: 90%; margin: auto; margin-top: 30px; margin-bottom: 30px;">
    <div class="main-heading" style="text-align: center;">
      <h1>@isset($replaceParamsArr['binderHolderletterhead']) {{ $replaceParamsArr['binderHolderletterhead'] }}@endisset</h1>
    </div>
    <div class="address">
      <address>
        @isset($replaceParamsArr['addressLine1']) {{ $replaceParamsArr['addressLine1'] }}@endisset, </br>
        @isset($replaceParamsArr['addressLine2']) {{ $replaceParamsArr['addressLine2'] }}@endisset, </br>
        @isset($replaceParamsArr['addressLine3']) {{ $replaceParamsArr['addressLine3'] }}@endisset, </br>
      </address>
    </div>
    <div class="date" style="text-align: right;">
      <p>
        <span style="font-weight: 600;">Insert Date: </span>
        <span>@isset($replaceParamsArr['date']) {{ $replaceParamsArr['date'] }}@endisset</span>
        </p>
    </div>
    <div class="policy-number">
      <p>
        <span  style="font-weight: 600;">Policy Number: </span>
        <span>@isset($replaceParamsArr['policyNumber']) {{ $replaceParamsArr['policyNumber'] }}@endisset</span>
      </p>
    </div>
    <div class="dear">
      <p>
        <span  style="font-weight: 600;">Dear: </span>
        <span>@isset($replaceParamsArr['insured']) {{ $replaceParamsArr['insured'] }}@endisset
        @isset($replaceParamsArr['surname']) {{ $replaceParamsArr['surname'] }}@endisset </span>
      </p>
    </div>
    <div class="notification">
      <div class="notification-hedaing">
        <h4 style=" text-transform: uppercase; line-height: 47px; letter-spacing: 0.5px;"> RE: <span style="text-decoration: underline;" >
        NOTIFICATION OF TERMINATION OF YOUR HELLO PROTECT POLICY </span> </h4>
      </div>
      <div class="notification-list">
        <ol style="padding-left: 0px;">
          <li>


            <p style="display: flex; gap: 22px; line-height: 40px; margin-bottom: 5px;">
                You are currently the policyholder of a @isset($replaceParamsArr['policyName']) {{ $replaceParamsArr['policyName'] }}@endisset  (‘’the Policy”)
                underwritten by
                @isset($replaceParamsArr['underWrittenBy']) {{ $replaceParamsArr['underWrittenBy'] }} @endisset , FSP No.
                 @isset($replaceParamsArr['fspGuardrisk']) {{ $replaceParamsArr['fspGuardrisk'] }} @endisset (“Guardrisk”),
                 an authorised financial services provider
                and
                a licensed microinsurer in terms of the Insurance Act, which is in turn administered by Hello Protect
                (Pty)
                Ltd, FSP No  @isset($replaceParamsArr['fspHelloProtect']) {{ $replaceParamsArr['fspHelloProtect'] }} @endisset  (“Hello Protect”).
            </p>
          </li>
          <li>
            <p style="display: flex; gap: 22px; line-height: 40px; margin-bottom: 5px;">
              Your policy provides for a @isset($replaceParamsArr['policyDuration']) {{ $replaceParamsArr['policyDuration'] }} @endisset -day cover period as per the policy terms and
                conditions.
            </p>
          </li>
          <li>
            <p style="display: flex; gap: 22px; line-height: 40px; margin-bottom: 5px;">
            As such, this communication serves as formal written notice of the intention to
              terminate
              this policy at the expiry of the  @isset($replaceParamsArr['policyDuration']) {{ $replaceParamsArr['policyDuration'] }} @endisset day cover
              period as required by the relevant provisions of the Policy,
              and the Policyholder Protection Rules (Long-term Insurance), 2018.
            </p>
          </li>
          <li>
            <p style="display: flex; gap: 22px; line-height: 40px; margin-bottom: 5px;">
            We thus hereby confirm that the Policy will be terminated with effect from
             <b>  @isset($replaceParamsArr['endDate ']) {{ $replaceParamsArr['endDate '] }} @endisset </b>.
              Please take note that all cover and policy terms and conditions remain unchanged during this notice
              period.
            </p>
          </li>
          <li>
            <p style="display: flex; gap: 22px; line-height: 40px; margin-bottom: 5px;">
            Queries in relation to the termination of your policy and/or any other enquiries can be
              directed to:
            </p>
            </li>
        </ol>
        <ul style="list-style: none;padding-left: 0px;">
          <li >Hello Protect (Pty) Ltd </li>
          <li>

            <span style="width: 10%;">Telephone: </span>
            <span> <a href="tel:012 880 1800" style="text-decoration: none; color: #000;">
             @isset($replaceParamsArr['brokerTelephone']) {{ $replaceParamsArr['brokerTelephone'] }} @endisset
            </a></span>

          </li>
          <li>

            <span style="width: 10%;">E-mail: </span>
            <span><a href = "mailto: insurance@helloprotect.co.za " style="text-decoration: none; color: #000;">
             @isset($replaceParamsArr['brokerEmail']) {{ $replaceParamsArr['brokerEmail'] }} @endisset
             </a></span>

          </li>
        </ul>


      </div>
      <div class="faithfully" style="margin-top: 20px; margin-bottom: 20px;">
        <p>Yours faithfully,</p>
      </div>
      <div class="sign">
        <p> @isset($replaceParamsArr['signedBy']) {{ $replaceParamsArr['signedBy'] }} @endisset </p>
      </div>
      <div class="name" style="margin-top: 20px;">
        @isset($replaceParamsArr['yourFaithfully']) {{ $replaceParamsArr['yourFaithfully'] }} @endisset
      </div>
    </div>
  </div>
</body>

</html>
